#pragma once

#include "quaternion.hpp"

#include <math/math.hpp>

using namespace gid_tech::math;

namespace gid_tech::algebra
{
	using namespace std;

	template<typename T>
	Quaternion<T>::Quaternion()
	{
	}

	template<typename T>
	Quaternion<T>::Quaternion(Quaternion<T> const & other)
	{
		x = other.x;
		y = other.y;
		z = other.z;
		w = other.w;
	}

	template<typename T>
	Quaternion<T>::Quaternion(T x, T y, T z, T w) :
		x(x), y(y), z(z), w(w)
	{
	}

	template<typename T>
	Quaternion<T> & Quaternion<T>::operator+=(Quaternion<T> const & other)
	{
		x += other.x;
		y += other.y;
		z += other.z;
		w += other.w;
		return *this;
	}

	template<typename T>
	Quaternion<T> & Quaternion<T>::operator-=(Quaternion<T> const & other)
	{
		x -= other.x;
		y -= other.y;
		z -= other.z;
		w -= other.w;
		return *this;
	}

	template<typename T>
	Quaternion<T> Quaternion<T>::operator+(Quaternion<T> const & other)
	{
		return Quaternion<T>(*this) += other;
	}

	template<typename T>
	Quaternion<T> Quaternion<T>::operator-(Quaternion<T> const & other)
	{
		return Quaternion<T>(*this) -= other;
	}

	template<typename T>
	Quaternion<T> & Quaternion<T>::operator*=(T other)
	{
		x *= other.x;
		y *= other.y;
		z *= other.z;
		w *= other.w;
		return *this;
	}

	template<typename T>
	Quaternion<T> & Quaternion<T>::operator/=(T other)
	{
		x /= other.x;
		y /= other.y;
		z /= other.z;
		w /= other.w;
		return *this;
	}

	template<typename T>
	Quaternion<T> Quaternion<T>::operator*(T other) const
	{
		return Quaternion<T>(*this) *= other;
	}

	template<typename T>
	Quaternion<T> Quaternion<T>::operator/(T other) const
	{
		return Quaternion<T>(*this) /= other;
	}

	template<typename T>
	Quaternion<T> & Quaternion<T>::operator*=(Quaternion<T> const & other)
	{
		return *this = *this * other;
	}

	template<typename T>
	Quaternion<T> & Quaternion<T>::operator/=(Quaternion<T> const & other)
	{
		return *this = *this / other;
	}

	template<typename T>
	Quaternion<T> Quaternion<T>::operator*(Quaternion<T> const & other) const
	{
		Quaternion<T> ret;
		ret.w = w * other.w - vector3.Dot(other.vector3);
		ret.vector3 = vector3 * other.w + other.vector3 * w + vector3.Cross(other.vector3);
		return ret;
	}

	template<typename T>
	Quaternion<T> Quaternion<T>::operator/(Quaternion<T> const & other) const
	{
		return *this * other.Inverted();
	}

	template<typename T>
	Vector<T, 3> Quaternion<T>::operator*(Vector<T, 3> const & other) const
	{
		return (*this * Quaternion<T>(other.x, other.y, other.z, T(0)) * this->Inverted()).v;
	}

	template<typename T>
	b8 Quaternion<T>::operator==(Quaternion<T> const & other) const
	{
		return elements == other.elements;
	}
	template<typename T>
	b8 Quaternion<T>::operator!=(Quaternion<T> const & other) const
	{
		return elements != other.elements;
	}

	template<typename T>
	Quaternion<T> & Quaternion<T>::Invert()
	{
		vector3 *= T(-1);
		return *this;
	}

	template<typename T>
	Quaternion<T> & Quaternion<T>::Inverted() const
	{
		return Quaternion(*this).Invert();
	}

	template<typename T>
	T Quaternion<T>::LengthSquared() const
	{
		return vector4.LengthSquared();
	}

	template<typename T>
	T Quaternion<T>::Length() const
	{
		return Sqrt(LengthSquared());
	}

	template<typename T>
	Quaternion<T> & Quaternion<T>::Normalize()
	{
		vector4 *= T(1) / Length();
		return *this;
	}

	template<typename T>
	Quaternion<T> Quaternion<T>::Normalized() const
	{
		return Quaternion<T>(*this).Normalize();
	}

	template<typename T>
	Quaternion<T> Quaternion<T>::Identity()
	{
		return Quaternion(T(0), T(0), T(0), T(1));
	}

	template<typename T>
	Quaternion<T> Quaternion<T>::FromAxisAngle(Vector<T, 3> const & normal, T angle)
	{
		auto vector3 = normal.Normalized() * Sin(angle / T{2});
		auto w = Cos(angle / T{2});
		return Quaternion<T>(vector3.x, vector3.y, vector3.z, w).Normalize();
	}
}
