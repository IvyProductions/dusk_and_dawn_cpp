#include "transform.hpp"

#include <math/math.hpp>
#include <math/constants.hpp>

using namespace gid_tech::math;

namespace gid_tech::algebra
{
	using namespace std;

	template<typename T>
	Matrix<T, 4, 4> Transform<T>::Orthographic(T left, T right, T bottom, T top, T near, T far)
	{
		Matrix<T, 4, 4> ret;

		ret[0][0] = +2.0 / (right - left);
		ret[1][1] = +2.0 / (top - bottom);
		ret[2][2] = -2.0 / (far - near);
		ret[3][3] = 1.0;

		ret[3][0] = -(right + left) / (right - left);
		ret[3][1] = -(top + bottom) / (top - bottom);
		ret[3][2] = -(far + near) / (far - near);

		return ret;
	}

	template<typename T>
	Matrix<T, 4, 4> Transform<T>::Perspective(T fov, T aspect, T near, T far)
	{
		Matrix<T, 4, 4> ret;

		//f32 q = 1.0 / tan(fov * 0.5);

		ret[0][0] = 1.0 / (Tan(fov * 0.5) * aspect);
		ret[1][1] = -1.0 / Tan(fov * 0.5);
		ret[2][2] = -(far + near) / (far - near);

		ret[3][2] = -(2.0 * far * near) / (far - near);
		ret[2][3] = -1.0;

		return ret;
	}

	template<typename T>
	Matrix<T, 4, 4> Transform<T>::Translation(Vector<T, 3> const & translation)
	{
		auto ret = Matrix<T, 4, 4>::Identity();

		ret[3][0] = translation.x;
		ret[3][1] = translation.y;
		ret[3][2] = translation.z;

		return ret;
	}

	template<typename T>
	Matrix<T, 4, 4> Transform<T>::Rotation(Quaternion<T> const & quaternion)
	{
		auto ret = Matrix<T, 4, 4>::Identity();

		auto x = quaternion.x;
		auto y = quaternion.y;
		auto z = quaternion.z;
		auto w = quaternion.w;

		auto x2 = x * x;
		auto y2 = y * y;
		auto z2 = z * z;

		ret[0][0] = 1 - 2 * y2 - 2 * z2;
		ret[0][1] = 2 * x * y + 2 * z * w;
		ret[0][2] = 2 * x * z - 2 * y * w;

		ret[1][0] = 2 * x * y - 2 * z * w;
		ret[1][1] = 1 - 2 * x2 - 2 * z2;
		ret[1][2] = 2 * y * z + 2 * x * w;

		ret[2][0] = 2 * x * z + 2 * y * w;
		ret[2][1] = 2 * y * z - 2 * x * w;
		ret[2][2] = 1 - 2 * x2 - 2 * y2;

		return ret;
	}

	template<typename T>
	Matrix<T, 4, 4> Transform<T>::Scale(Vector<T, 3> const & scale)
	{
		auto ret = Matrix<T, 4, 4>::Identity();

		ret[0][0] = scale.x;
		ret[1][1] = scale.y;
		ret[2][2] = scale.z;

		return ret;
	}

	template<typename T>
	Matrix<T, 4, 4> Transform<T>::TRS(Vector<T, 3> const & position, Quaternion<T> const & rotation, Vector<T, 3> const & scale)
	{
		return Translation(position) * Rotation(rotation) * Scale(scale);
	}

	template<typename T>
	Vector<T, 3> Transform<T>::MatrixToPosition(Matrix<T, 4, 4> const & matrix)
	{
		return Vector<T, 3>(matrix[3][0], matrix[3][1], matrix[3][2]);
	}

	template<typename T>
	Quaternion<T> Transform<T>::MatrixToRotation(Matrix<T, 4, 4> const & matrix)
	{
		Quaternion<T> ret;

		ret.w = sqrt(max(0.f, 1 + matrix[0][0] + matrix[1][1] + matrix[2][2])) / 2.f;
		ret.x = sqrt(max(0.f, 1 + matrix[0][0] - matrix[1][1] - matrix[2][2])) / 2.f;
		ret.y = sqrt(max(0.f, 1 - matrix[0][0] + matrix[1][1] - matrix[2][2])) / 2.f;
		ret.z = sqrt(max(0.f, 1 - matrix[0][0] - matrix[1][1] + matrix[2][2])) / 2.f;

		ret.x = copysign(ret.x, matrix[1][2] - matrix[2][1]);
		ret.y = copysign(ret.y, matrix[2][0] - matrix[0][2]);
		ret.z = copysign(ret.z, matrix[0][1] - matrix[1][0]);

		return ret;
	}

	template<typename T>
	Vector<T, 3> Transform<T>::MatrixToScale(Matrix<T, 4, 4> const & matrix)
	{
		return Vector<T, 3>(matrix[0][0], matrix[1][1], matrix[2][2]);
	}

	template<typename T>
	Vector<T, 3> Transform<T>::QuaternionToEuler(Quaternion<T> const & q)
	{
		Vector<T, 3> ret;

		T sinx = 2.0 * (q.w * q.x + q.y * q.z);
		T cosx = 1.0 - 2.0 * (q.x * q.x + q.y * q.y);
		ret.x = Atan2(sinx, cosx);

		T siny = +2.0 * (q.w * q.y - q.z * q.x);
		ret.y = Abs(siny) >= 1 ? ret.y = CopySign(pi / 2, siny) : ret.y = Asin(siny);

		T sinz = +2.0 * (q.w * q.z + q.x * q.y);
		T cosz = +1.0 - 2.0 * (q.y * q.y + q.z * q.z);
		ret.z = Atan2(sinz, cosz);

		return ret;
	}

	template<typename T>
	Quaternion<T> Transform<T>::EulerToQuaternion(Vector<T, 3> const & euler)
	{
		return Quaternion<T>::FromAxisAngle({0, 0, 1}, euler.x) * Quaternion<T>::FromAxisAngle({0, 1, 0}, euler.y) * Quaternion<T>::FromAxisAngle({1, 0, 0}, euler.z);
	}

	template<typename T>
	Vector<T, 3> Transform<T>::Forward()
	{
		return Vector<T, 3>(0, 0, +1);
	}

	template<typename T>
	Vector<T, 3> Transform<T>::Backward()
	{
		return Vector<T, 3>(0, 0, -1);
	}

	template<typename T>
	Vector<T, 3> Transform<T>::Left()
	{
		return Vector<T, 3>(+1, 0, 0);
	}

	template<typename T>
	Vector<T, 3> Transform<T>::Right()
	{
		return Vector<T, 3>(-1, 0, 0);
	}

	template<typename T>
	Vector<T, 3> Transform<T>::Up()
	{
		return Vector<T, 3>(0, +1, 0);
	}

	template<typename T>
	Vector<T, 3> Transform<T>::Down()
	{
		return Vector<T, 3>(0, -1, 0);
	}
}
