#pragma once

#include <types/traits.hpp>

#include "matrix.hpp"
#include "quaternion.hpp"
#include "vector.hpp"

using namespace gid_tech::types;
using namespace std;

namespace gid_tech::algebra
{
	template<typename T>
	struct Transform
	{
		static_assert(IsFloating<T>::value);

		static Matrix<T, 4, 4> Orthographic(T left, T right, T bottom, T top, T near, T far);
		static Matrix<T, 4, 4> Perspective(T fov, T apsect, T near, T far);

		static Matrix<T, 4, 4> Translation(Vector<T, 3> const & translation);
		static Matrix<T, 4, 4> Rotation(Quaternion<T> const & quaternion);
		static Matrix<T, 4, 4> Scale(Vector<T, 3> const & scale);
		static Matrix<T, 4, 4> TRS(Vector<T, 3> const & position, Quaternion<T> const & rotation, Vector<T, 3> const & scale);

		static Vector<T, 3> MatrixToPosition(Matrix<T, 4, 4> const & matrix);
		static Quaternion<T> MatrixToRotation(Matrix<T, 4, 4> const & matrix);
		static Vector<T, 3> MatrixToScale(Matrix<T, 4, 4> const & matrix);

		static Vector<T, 3> QuaternionToEuler(Quaternion<T> const & quaternion);
		static Quaternion<T> EulerToQuaternion(Vector<T, 3> const & vector);

		static Vector<T, 3> Forward();
		static Vector<T, 3> Backward();
		static Vector<T, 3> Left();
		static Vector<T, 3> Right();
		static Vector<T, 3> Up();
		static Vector<T, 3> Down();
	};
}

#include "transform.impl.hpp"
