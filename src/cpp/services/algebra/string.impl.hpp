#pragma once

#include <sstream>

#include "string.hpp"

namespace gid_tech::algebra
{
	using std::fixed;
	using std::is_f32ing_point_v;
	using std::stringstream;

	template<typename T, usize X, usize Y>
	string ToString(Matrix<T, X, Y> const & matrix)
	{
		stringstream ss;
		ss << fixed;

		for (usize i = 0; i < X; i++)
		{
			ss << "[";
			for (usize j = 0; j < Y; j++)
			{
				auto v = matrix.columns[j][i];
				if (v >= 0 && is_f32ing_point_v<T>)
					ss << "+";
				ss << v;
				if (j < Y - 1)
					ss << " ,";
			}
			ss << "]";

			if (i < X - 1)
				ss << '\n';
		}

		return ss.str();
	}

	template<typename T, usize X>
	string ToString(Vector<T, X> const & vector)
	{
		stringstream ss;
		ss << fixed;

		ss << "[";
		for (usize i = 0; i < X; i++)
		{
			auto v = vector.elements[i];
			if (v >= 0 && is_f32ing_point_v<T>)
				ss << "+";
			ss << v;
			if (i < X - 1)
				ss << " ,";
		}
		ss << "]";

		return ss.str();
	}

	template<typename T>
	string ToString(Quaternion<T> const & quaternion)
	{
		return ToString(quaternion.vector4);
	}
}
