#include "image.hpp"

#include <fstream>

#include <image_decoding/decode.hpp>

namespace gid_tech::loading::image
{
	shared_ptr<Image> Load(path path, RenderManager & render_manager)
	{
		ifstream image_file(path, ios::binary);
		vector<u8> image_data((istreambuf_iterator<char>(image_file)), istreambuf_iterator<char>());
		auto [pixels, extent] = image_decoding::DecodeTo<data::Pixel>(image_data);
		auto image_extent = Size{(u32) extent.width, (u32) extent.height};
		return make_shared<Image>(render_manager, image_extent, move(pixels));
	}
}
