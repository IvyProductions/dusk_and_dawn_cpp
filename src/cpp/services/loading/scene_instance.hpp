#pragma once

#include "scene_elements.hpp"

namespace gid_tech::loading
{
	using SceneInstance = tuple<vector<Node *>, vector<shared_ptr<Node>>, shared_ptr<SceneElements>>;
	SceneInstance InstantiateSceneElements(shared_ptr<SceneElements> elements, Optional<variant<Scene *, Node *>> scene_or_parent);
}
