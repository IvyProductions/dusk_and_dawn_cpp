#include "cache.hpp"

#include "image.hpp"
#include "pipeline.hpp"
#include "scene.hpp"

namespace gid_tech::loading
{
	LoadingCache::LoadingCache(
		RenderManager & render_manager,
		function<shared_ptr<Pipeline>(string const &, MaterialData const &)> pipeline_selector,
		function<shared_ptr<Frame>(string const &)> frame_selector) :
		render_manager(render_manager),
		pipeline_selector(pipeline_selector),
		frame_selector(frame_selector)
	{
	}

	shared_ptr<Pipeline> LoadingCache::GetOrLoadPipeline(path path_vert, path path_frag, u32 image_count, bool is_transparent)
	{
		auto key = make_tuple(path_vert, path_frag, image_count, is_transparent);
		if (auto cached = pipelines[key].lock())
			return cached;
		else
		{
			auto ret = pipeline::Load(path_vert, path_frag, image_count, is_transparent, render_manager);
			pipelines[key] = ret;
			return ret;
		}
	}

	shared_ptr<Image> LoadingCache::GetOrLoadImage(path path)
	{
		auto key = path;
		if (auto cached = images[key].lock())
			return cached;
		else
		{
			auto ret = image::Load(path, render_manager);
			images[key] = ret;
			return ret;
		}
	}

	shared_ptr<SceneElements> LoadingCache::GetOrLoadScene(path path)
	{
		auto key = path;
		if (auto cached = scenes[key].lock())
			return cached;
		else
		{
			auto ret = scene::Load(
				path,
				render_manager,
				pipeline_selector,
				frame_selector,
				[&](filesystem::path const & p) { return GetOrLoadImage(p); });
			scenes[key] = ret;
			return ret;
		}
	}
}
