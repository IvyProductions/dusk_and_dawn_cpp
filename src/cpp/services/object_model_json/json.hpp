#pragma once

#include <optional>
#include <string>

#include <object_model/object_model.hpp>

namespace gid_tech::object_model::json
{
	using namespace std;
	using namespace object_model;

	optional<Object> JsonToObject(string const & json);
	string ObjectToJson(Object const & value);
}
