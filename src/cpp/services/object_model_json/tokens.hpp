#pragma once

#include <string>

#include <types/types.hpp>

using namespace gid_tech::types;

namespace gid_tech::object_model::json::tokens
{
	using namespace std;

	struct Null // 0
	{
	};
	struct Bool // 1
	{
		bool value;
	};
	struct Int // 2
	{
		isize value;
	};
	struct Float // 3
	{
		f64 value;
	};
	struct String // 4
	{
		string value;
	};
	struct Comma // 5
	{
	};
	struct Colon // 6
	{
	};
	struct LeftBracket // 7
	{
	};
	struct RightBracket // 8
	{
	};
	struct LeftBrace // 9
	{
	};
	struct RightBrace // 10
	{
	};
}
