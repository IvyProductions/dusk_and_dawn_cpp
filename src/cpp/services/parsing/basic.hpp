#pragma once

#include <types/types.hpp>

using namespace gid_tech::types;

namespace gid_tech::parsing
{
	b8 ToB8(c8 const * str);
	u64 ToU64(c8 const * str);
	f64 ToF64(c8 const * str);

	inline u32 ToU32(c8 const * str)
	{
		return static_cast<u32>(ToU64(str));
	}

	inline u16 ToU16(c8 const * str)
	{
		return static_cast<u16>(ToU64(str));
	}

	inline u8 ToU8(c8 const * str)
	{
		return static_cast<u8>(ToU64(str));
	}

	inline i64 ToI64(c8 const * str)
	{
		auto ret = ToU64(str);
		return *reinterpret_cast<i64 *>(&ret);
	}

	inline i32 ToI32(c8 const * str)
	{
		return static_cast<i32>(ToI64(str));
	}

	inline i16 ToI16(c8 const * str)
	{
		return static_cast<i16>(ToI64(str));
	}

	inline i8 ToI8(c8 const * str)
	{
		return static_cast<i8>(ToI64(str));
	}

	inline i64 ToF32(c8 const * str)
	{
		return static_cast<f64>(ToF64(str));
	}
}
