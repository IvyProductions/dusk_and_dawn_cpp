#pragma once

#include "types.hpp"

namespace gid_tech::types
{
	template<typename T>
	constexpr T min()
	{
		return 0;
	}
	template<>
	constexpr i8 min<i8>()
	{
		return -128;
	}
	template<>
	constexpr i16 min<i16>()
	{
		return -32768;
	}
	template<>
	constexpr i32 min<i32>()
	{
		return -2147483648;
	}
	template<>
	constexpr i64 min<i64>()
	{
		return -9223372036854775808;
	}
	template<>
	constexpr u8 min<u8>()
	{
		return 0;
	}
	template<>
	constexpr u16 min<u16>()
	{
		return 0;
	}
	template<>
	constexpr u32 min<u32>()
	{
		return 0;
	}
	template<>
	constexpr u64 min<u64>()
	{
		return 0;
	}

	template<typename T>
	constexpr T max()
	{
		return 0;
	}
	template<>
	constexpr i8 max<i8>()
	{
		return 127;
	}
	template<>
	constexpr i16 max<i16>()
	{
		return 32767;
	}
	template<>
	constexpr i32 max<i32>()
	{
		return 2147483647;
	}
	template<>
	constexpr i64 max<i64>()
	{
		return 9223372036854775807;
	}
	template<>
	constexpr u8 max<u8>()
	{
		return 255;
	}
	template<>
	constexpr u16 max<u16>()
	{
		return 65535;
	}
	template<>
	constexpr u32 max<u32>()
	{
		return 4294967295;
	}
	template<>
	constexpr u64 max<u64>()
	{
		return 18446744073709551615;
	}
}
