#pragma once

namespace gid_tech::types
{
	using b8 = bool;
	using c8 = char;

	using i8 = signed char;
	using u8 = unsigned char;
	using i16 = signed short int;
	using u16 = unsigned short int;
	using i32 = signed int;
	using u32 = unsigned int;
#ifdef WINDOWS
	using i64 = signed long long int;
	using u64 = unsigned long long int;
#elif LINUX
	using i64 = signed long int;
	using u64 = unsigned long int;
#endif

	using isize = i64;
	using usize = u64;

	using f32 = float;
	using f64 = double;

#define WRONG_SIZE_ERROR "wrong size"

	static_assert(sizeof(b8) == 1, WRONG_SIZE_ERROR);
	static_assert(sizeof(c8) == 1, WRONG_SIZE_ERROR);

	static_assert(sizeof(i8) == 1, WRONG_SIZE_ERROR);
	static_assert(sizeof(u8) == 1, WRONG_SIZE_ERROR);
	static_assert(sizeof(i16) == 2, WRONG_SIZE_ERROR);
	static_assert(sizeof(u16) == 2, WRONG_SIZE_ERROR);
	static_assert(sizeof(i32) == 4, WRONG_SIZE_ERROR);
	static_assert(sizeof(u32) == 4, WRONG_SIZE_ERROR);
	static_assert(sizeof(i64) == 8, WRONG_SIZE_ERROR);
	static_assert(sizeof(u64) == 8, WRONG_SIZE_ERROR);

	static_assert(sizeof(f32) == 4, WRONG_SIZE_ERROR);
	static_assert(sizeof(f64) == 8, WRONG_SIZE_ERROR);

	
	// user literals

	inline b8 operator"" _B8(unsigned long long v)
	{
		return static_cast<b8>(v);
	}
	inline c8 operator"" _C8(unsigned long long v)
	{
		return static_cast<c8>(v);
	}

	inline i8 operator"" _I8(unsigned long long v)
	{
		return static_cast<i8>(v);
	}
	inline u8 operator"" _U8(unsigned long long v)
	{
		return static_cast<u8>(v);
	}
	inline i16 operator"" _I16(unsigned long long v)
	{
		return static_cast<i16>(v);
	}
	inline u16 operator"" _U16(unsigned long long v)
	{
		return static_cast<u16>(v);
	}
	inline i32 operator"" _I32(unsigned long long v)
	{
		return static_cast<i32>(v);
	}
	inline u32 operator"" _U32(unsigned long long v)
	{
		return static_cast<u32>(v);
	}
	inline i64 operator"" _I64(unsigned long long v)
	{
		return static_cast<i8>(v);
	}
	inline u64 operator"" _U64(unsigned long long v)
	{
		return static_cast<u64>(v);
	}

	inline isize operator"" _ISIZE(unsigned long long v)
	{
		return v;
	}
	inline usize operator"" _USIZE(unsigned long long v)
	{
		return v;
	}

	inline f32 operator"" _F32(long double v)
	{
		return static_cast<f32>(v);
	}
	inline f64 operator"" _F64(long double v)
	{
		return static_cast<f64>(v);
	}
}
