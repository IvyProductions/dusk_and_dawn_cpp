#pragma once

#include <types/types.hpp>

#undef True
#undef False
#undef Bool

namespace gid_tech::types
{
#pragma region // value

	template<typename T, T v>
	struct Value
	{
		static constexpr T value = v;
	};

#pragma endregion

#pragma region // boolean

	template<b8 v>
	using Bool = Value<b8, v>;
	using True = Bool<true>;
	using False = Bool<false>;

	template<b8, typename A, typename B>
	struct If
	{
		using Type = A;
	};
	template<typename A, typename B>
	struct If<false, A, B>
	{
		using Type = B;
	};

	template<typename B, typename... Tail>
	struct And : If<B::value, And<Tail...>, False>::Type
	{
	};
	template<typename B>
	struct And<B> : B
	{
	};

	template<typename B, typename... Tail>
	struct Or : If<B::value, True, Or<Tail...>>::Type
	{
	};
	template<typename B>
	struct Or<B> : B
	{
	};

	template<typename B>
	struct Not : Bool<!B::value>
	{
	};

#pragma endregion

#pragma region // conditional compilation

	template<b8 B, typename T = void>
	struct EnableIf
	{
	};

	template<typename T>
	struct EnableIf<true, T>
	{
		using Type = T;
	};

#pragma endregion

#pragma region // modifications:

	template<typename T>
	struct RemoveReference
	{
		using Type = T;
	};
	template<typename T>
	struct RemoveReference<T &>
	{
		using Type = T;
	};
	template<typename T>
	struct RemoveReference<T &&>
	{
		using Type = T;
	};

	template<typename T>
	struct RemovePointer
	{
		using Type = T;
	};
	template<typename T>
	struct RemovePointer<T *>
	{
		using Type = T;
	};

	template<typename T>
	struct RemoveConst
	{
		using Type = T;
	};
	template<typename T>
	struct RemoveConst<T const>
	{
		using Type = T;
	};

	template<typename T>
	struct RemoveVolatile
	{
		using Type = T;
	};
	template<typename T>
	struct RemoveVolatile<T volatile>
	{
		using Type = T;
	};

	template<typename T>
	using RemoveModifiers = RemoveConst<typename RemoveVolatile<T>::Type>;

	template<typename T>
	struct AddSign
	{
	};
	template<>
	struct AddSign<u8>
	{
		using Type = i8;
	};
	template<>
	struct AddSign<u16>
	{
		using Type = i16;
	};
	template<>
	struct AddSign<u32>
	{
		using Type = i32;
	};
	template<>
	struct AddSign<u64>
	{
		using Type = i64;
	};
	template<>
	struct AddSign<i8>
	{
		using Type = i8;
	};
	template<>
	struct AddSign<i16>
	{
		using Type = i16;
	};
	template<>
	struct AddSign<i32>
	{
		using Type = i32;
	};
	template<>
	struct AddSign<i64>
	{
		using Type = i64;
	};

	template<typename T>
	struct RemoveSign
	{
	};
	template<>
	struct RemoveSign<u8>
	{
		using Type = u8;
	};
	template<>
	struct RemoveSign<u16>
	{
		using Type = u16;
	};
	template<>
	struct RemoveSign<u32>
	{
		using Type = u32;
	};
	template<>
	struct RemoveSign<u64>
	{
		using Type = u64;
	};
	template<>
	struct RemoveSign<i8>
	{
		using Type = u8;
	};
	template<>
	struct RemoveSign<i16>
	{
		using Type = u16;
	};
	template<>
	struct RemoveSign<i32>
	{
		using Type = u32;
	};
	template<>
	struct RemoveSign<i64>
	{
		using Type = u64;
	};

	template<typename T>
	struct ToInteger
	{
	};
	template<>
	struct ToInteger<f32>
	{
		using Type = i32;
	};
	template<>
	struct ToInteger<f64>
	{
		using Type = i64;
	};

	template<typename T>
	struct ToFloating
	{
	};
	template<>
	struct ToFloating<i32>
	{
		using Type = f32;
	};
	template<>
	struct ToFloating<i64>
	{
		using Type = f64;
	};

#pragma endregion

#pragma region // type comparisons

	template<typename, typename>
	struct Is : False
	{
	};
	template<typename T>
	struct Is<T, T> : True
	{
	};

	template<typename T, typename... Tail>
	struct IsAny : Or<Is<T, Tail>...>
	{
	};

	template<typename T>
	struct IsVoid : Is<void, T>
	{
	};

	template<typename T>
	struct IsBoolean : Is<typename RemoveModifiers<T>::Type, b8>
	{
	};

	template<typename T>
	struct IsChar : Is<typename RemoveModifiers<T>::Type, c8>
	{
	};

	template<typename T>
	struct IsInteger : IsAny<typename RemoveModifiers<T>::Type, i8, i16, i32, i64, u8, u16, u32, u64>
	{
	};

	template<typename T>
	struct IsFloating : IsAny<typename RemoveModifiers<T>::Type, f32, f64>
	{
	};

	template<typename T>
	struct IsPointer : False
	{
	};
	template<typename T>
	struct IsPointer<T *> : True
	{
	};

	template<typename T>
	struct IsLReference : False
	{
	};
	template<typename T>
	struct IsLReference<T &> : True
	{
	};
	template<typename T>
	struct IsRReference : False
	{
	};
	template<typename T>
	struct IsRReference<T &&> : True
	{
	};
	template<typename T>
	using IsReference = Or<IsLReference<T>, IsRReference<T>>;

	template<typename T>
	struct IsArray : False
	{
	};
	template<typename T>
	struct IsArray<T[]> : True
	{
	};
	template<typename T, u64 c>
	struct IsArray<T[c]> : True
	{
	};

	template<typename T>
	struct IsIntegral : Or<IsBoolean<T>, IsChar<T>, IsInteger<T>, IsFloating<T>>
	{
	};
	template<typename T>
	struct IsEnum : Bool<__is_enum(T)>
	{
	};
	template<typename T>
	struct IsUnion : Bool<__is_union(T)>
	{
	};
	template<typename T>
	struct IsClass : Bool<__is_class(T)>
	{
	};

	template<typename T>
	struct IsFunction : False
	{
	};
	template<typename T, typename... TArgs>
	struct IsFunction<T(TArgs...)> : True
	{
	};

	template<typename T>
	struct IsTrivialConstructible : Bool<__is_trivially_constructible(T)>
	{
	};
	template<typename T>
	struct IsTrivialCopyable : Bool<__is_trivially_copyable(T)>
	{
	};
	template<typename T>
	struct IsTrivial : Bool<IsTrivialConstructible<T>::value && IsTrivialCopyable<T>::value>
	{
	};

#pragma endregion

#pragma region // type operation support

	template<typename T>
	struct ArrayElementType
	{
	};
	template<typename T>
	struct ArrayElementType<T[]>
	{
		using Type = T;
	};
	template<typename T, u64 S>
	struct ArrayElementType<T[S]>
	{
		using Type = T;
	};

	template<typename T>
	struct ArrayElementCount
	{
	};
	template<typename T, u64 S>
	struct ArrayElementCount<T[S]>
	{
		static constexpr u64 value = S;
	};

#pragma endregion

}
