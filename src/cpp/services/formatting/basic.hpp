#pragma once

#include <collections/array.hpp>
#include <types/types.hpp>

using namespace gid_tech::types;
using namespace gid_tech::collections;

namespace gid_tech::formatting
{
	struct Parse
	{
		usize length{};
		Array<c8, 24> chars{};
		void Write(c8 c);
		void Write(c8 const * str);
		void SetLenght(usize len);
	};

	Parse FromB8(b8 v);
	Parse FromU64(u64 v, usize base = 10);
	Parse FromI64(i64 v, usize base = 10);
	Parse FromF64(f64 v);

	inline Parse FromU32(u32 v, usize base = 10)
	{
		return FromU64(v, base);
	}
	inline Parse FromU16(u16 v, usize base = 10)
	{
		return FromU64(v, base);
	}
	inline Parse FromU8(u8 v, usize base = 10)
	{
		return FromU64(v, base);
	}

	inline Parse FromI32(i32 v, usize base = 10)
	{
		return FromI64(v, base);
	}
	inline Parse FromI16(i16 v, usize base = 10)
	{
		return FromI64(v, base);
	}
	inline Parse FromI8(i8 v, usize base = 10)
	{
		return FromI64(v, base);
	}

	inline Parse FromF32(f32 v)
	{
		return FromF64(v);
	}
}
