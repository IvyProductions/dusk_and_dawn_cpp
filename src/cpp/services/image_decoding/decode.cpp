#include "decode.hpp"

#include <utility>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.hpp"

#include <types/types.hpp>

using namespace gid_tech::types;

namespace gid_tech::image_decoding
{
	tuple<vector<Pixel>, Extent> Decode(vector<u8> const & data)
	{
		int x{}, y{}, n{};
		auto loaded = stbi_load_from_memory(data.data(), data.size(), &x, &y, &n, 4);
		vector<Pixel> pixels(x * y);
		memcpy(pixels.data(), loaded, static_cast<usize>(x * y * sizeof(Pixel)));
		stbi_image_free(loaded);
		return tuple<vector<Pixel>, Extent>(move(pixels), Extent{.width = static_cast<usize>(x), .height = static_cast<usize>(y)});
	}
}
