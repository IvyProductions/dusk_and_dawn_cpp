#pragma once

#include <functional>
#include <optional>
#include <string>
#include <tuple>
#include <types/types.hpp>
#include <variant>
#include <vector>

using namespace std;
using namespace gid_tech::types;

namespace gid_tech::object_model
{
	class Object
	{
	private:
		using Values = variant<
			b8,
			i64,
			f64,
			string,
			vector<Object>,
			vector<tuple<string, Object>>>;

	private:
		optional<Values> value;

	public:
		Object();
		Object(bool v);
		Object(int64_t v);
		Object(double v);
		Object(string v);

		Object(Object const & other);
		Object(Object && other);

		Object & operator=(Object const & other);
		Object & operator=(Object && other);


		bool IsNull() const;

		optional<b8> AsBool() const;
		optional<i64> AsInt() const;
		optional<u64> AsUInt() const;
		optional<f32> AsFloat() const;
		optional<f64> AsDouble() const;
		optional<string> AsString() const;

		optional<usize> GetArrayCount() const;
		optional<Object *> GetArrayValue(usize index);
		optional<Object const *> GetArrayValue(usize index) const;

		optional<usize> GetObjectCount() const;
		optional<string const *> GetObjectName(usize index) const;
		optional<Object *> GetObjectValue(usize index);
		optional<Object const *> GetObjectValue(usize index) const;
		optional<Object *> GetObjectValue(string const & name);
		optional<Object const *> GetObjectValue(string const & name) const;


		void SetValue();
		void SetValue(bool v);
		void SetValue(int64_t v);
		void SetValue(double v);
		void SetValue(string v);

		void AddArrayValue(Object v);
		void RemoveArrayValue(usize index);

		void AddObjectValue(string const & name, Object v);
		void RemoveObjectValue(string const & name);

	private:
		template<typename T>
		optional<T *> Get();
		template<typename T>
		optional<T const *> Get() const;
	};

	template<typename T>
	optional<T *> Object::Get()
	{
		if (value.has_value())
		{
			if (auto * v = get_if<T>(&value.value()))
				return make_optional(v);
			else
				return nullopt;
		}
		else
			return nullopt;
	}

	template<typename T>
	optional<T const *> Object::Get() const
	{
		if (value.has_value())
		{
			if (auto * v = get_if<T>(&value.value()))
				return make_optional(v);
			else
				return nullopt;
		}
		else
			return nullopt;
	}
}
