#include "object_model.hpp"
namespace gid_tech::object_model
{
	Object::Object() :
		value(nullopt)
	{
	}
	Object::Object(bool v) :
		value(make_optional<Values>(v))
	{
	}
	Object::Object(int64_t v) :
		value(make_optional<Values>(v))
	{
	}
	Object::Object(double v) :
		value(make_optional<Values>(v))
	{
	}
	Object::Object(string v) :
		value(make_optional<Values>(v))
	{
	}

	Object::Object(Object const & other) :
		value(other.value)
	{
	}
	Object::Object(Object && other) :
		value(move(other.value))
	{
	}

	Object & Object::operator=(Object const & other)
	{
		value = other.value;
		return *this;
	}

	Object & Object::operator=(Object && other)
	{
		value = move(other.value);
		return *this;
	}

	bool Object::IsNull() const
	{
		return !value.has_value();
	}

	optional<bool> Object::AsBool() const
	{
		if (auto b = Get<bool>())
			return make_optional((bool) *b.value());
		else if (auto i = Get<int64_t>())
			return make_optional((bool) *i.value());
		else if (auto d = Get<double>())
			return make_optional((bool) *d.value());
		else
			return nullopt;
	}

	optional<int64_t> Object::AsInt() const
	{
		if (auto b = Get<bool>())
			return make_optional((int64_t) *b.value());
		else if (auto i = Get<int64_t>())
			return make_optional((int64_t) *i.value());
		else if (auto d = Get<double>())
			return make_optional((int64_t) *d.value());
		else
			return nullopt;
	}

	optional<u64> Object::AsUInt() const
	{
		if (auto b = Get<bool>())
			return make_optional((u64) *b.value());
		else if (auto i = Get<int64_t>())
			return make_optional((u64) *i.value());
		else if (auto d = Get<double>())
			return make_optional((u64) *d.value());
		else
			return nullopt;
	}

	optional<f32> Object::AsFloat() const
	{
		if (auto b = Get<bool>())
			return make_optional((f32) *b.value());
		else if (auto i = Get<int64_t>())
			return make_optional((f32) *i.value());
		else if (auto d = Get<double>())
			return make_optional((f32) *d.value());
		else
			return nullopt;
	}

	optional<double> Object::AsDouble() const
	{
		if (auto b = Get<bool>())
			return make_optional((double) *b.value());
		else if (auto i = Get<int64_t>())
			return make_optional((double) *i.value());
		else if (auto d = Get<double>())
			return make_optional((double) *d.value());
		else
			return nullopt;
	}

	optional<string> Object::AsString() const
	{
		if (auto s = Get<string>())
			return make_optional(*s.value());
		else
			return nullopt;
	}

	optional<usize> Object::GetArrayCount() const
	{
		if (auto a = Get<vector<Object>>())
			return make_optional(a.value()->size());
		else
			return nullopt;
	}

	optional<Object *> Object::GetArrayValue(usize index)
	{
		if (auto a = Get<vector<Object>>())
		{
			if (index < a.value()->size())
				return make_optional(&(*a.value())[index]);
			else
				return nullopt;
		}
		else
			return nullopt;
	}

	optional<Object const *> Object::GetArrayValue(usize index) const
	{
		if (auto a = Get<vector<Object>>())
		{
			if (index < a.value()->size())
				return make_optional(&(*a.value())[index]);
			else
				return nullopt;
		}
		else
			return nullopt;
	}

	optional<usize> Object::GetObjectCount() const
	{
		if (auto o = Get<vector<tuple<string, Object>>>())
			return make_optional(o.value()->size());
		else
			return nullopt;
	}

	optional<string const *> Object::GetObjectName(usize index) const
	{
		if (auto o = Get<vector<tuple<string, Object>>>())
		{
			if (index < o.value()->size())
				return make_optional(&get<0>((*o.value())[index]));
			else
				return nullopt;
		}
		else
			return nullopt;
	}

	optional<Object *> Object::GetObjectValue(usize index)
	{
		if (auto o = Get<vector<tuple<string, Object>>>())
		{
			if (index < o.value()->size())
				return make_optional(&get<1>((*o.value())[index]));
			else
				return nullopt;
		}
		else
			return nullopt;
	}

	optional<Object const *> Object::GetObjectValue(usize index) const
	{
		if (auto o = Get<vector<tuple<string, Object>>>())
		{
			if (index < o.value()->size())
				return make_optional(&get<1>((*o.value())[index]));
			else
				return nullopt;
		}
		else
			return nullopt;
	}

	optional<Object *> Object::GetObjectValue(string const & name)
	{
		if (auto a = Get<vector<tuple<string, Object>>>())
		{
			for (auto & [n, v] : *a.value())
			{
				if (n == name)
					return make_optional(&v);
			}
			return nullopt;
		}
		else
			return nullopt;
	}

	optional<Object const *> Object::GetObjectValue(string const & name) const
	{
		if (auto o = Get<vector<tuple<string, Object>>>())
		{
			for (auto & [n, v] : *o.value())
			{
				if (n == name)
					return make_optional(&v);
			}
			return nullopt;
		}
		else
			return nullopt;
	}


	void Object::SetValue()
	{
		value = nullopt;
	}

	void Object::SetValue(bool v)
	{
		value = make_optional<Values>(v);
	}

	void Object::SetValue(int64_t v)
	{
		value = make_optional<Values>(v);
	}

	void Object::SetValue(double v)
	{
		value = make_optional<Values>(v);
	}

	void Object::SetValue(string v)
	{
		value = make_optional<Values>(v);
	}

	void Object::AddArrayValue(Object value)
	{
		if (auto a = Get<vector<Object>>())
			a.value()->emplace_back(move(value));
		else
			this->value = make_optional<Values>(vector<Object>{value});
	}

	void Object::RemoveArrayValue(usize index)
	{
		if (auto a = Get<vector<Object>>())
		{
			if (index < a.value()->size())
				a.value()->erase(a.value()->begin() + index);
		}
	}

	void Object::AddObjectValue(string const & name, Object value)
	{
		if (auto o = Get<vector<tuple<string, Object>>>())
		{
			for (auto & [n, v] : *o.value())
			{
				if (n == name)
					v = move(value);
			}
			o.value()->emplace_back(name, value);
		}
		else
		{
			this->value = make_optional<Values>(vector<tuple<string, Object>>{
				{name, value},
			});
		}
	}

	void Object::RemoveObjectValue(string const & name)
	{
		if (auto o = Get<vector<tuple<string, Object>>>())
		{
			usize i = 0;
			for (auto & [n, v] : *o.value())
			{
				if (n == name)
				{
					o.value()->erase(o.value()->begin() + i);
					break;
				}
				i += 1;
			}
		}
	}
}
