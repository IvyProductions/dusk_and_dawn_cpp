#ifdef WINDOWS

#include "../raw_alloc.hpp"

#include <windows.h>

namespace gid_tech::heap
{
	void * RawAlloc(usize size)
	{
		return HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, size);
	}

	void RawDealloc(void * ptr)
	{
		HeapFree(GetProcessHeap(), 0, ptr);
	}

	usize RawSize(void * ptr)
	{
		return HeapSize(GetProcessHeap(), 0, ptr);
	}
}

#endif
