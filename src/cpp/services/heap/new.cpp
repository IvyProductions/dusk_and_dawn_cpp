#include "new.hpp"
#include "allocator.hpp"

using namespace gid_tech::heap;

void * operator new(usize size)
{
	return Allocator::default_allocator.Alloc<u8>(size);
}

void * operator new[](usize size)
{
	return Allocator::default_allocator.Alloc<u8>(size);
}

void * operator new(usize size, void * where) noexcept
{
	return where;
}

void * operator new[](usize size, void * where) noexcept
{
	return where;
}

void operator delete(void * ptr) noexcept
{
	Allocator::default_allocator.Dealloc(ptr);
}

void operator delete[](void * ptr) noexcept
{
	Allocator::default_allocator.Dealloc(ptr);
}
