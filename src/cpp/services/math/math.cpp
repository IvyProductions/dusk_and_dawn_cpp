#include "math.hpp"

#include "constants.hpp"

extern "C"
{
	b8 __isnanf(f32 x);
	b8 isnan(f64 x);

	f32 sinf(f32 x);
	f64 sin(f64 x);
	f32 cosf(f32 x);
	f64 cos(f64 x);
	f32 tanf(f32 x);
	f64 tan(f64 x);
	f32 sinhf(f32 x);
	f64 sinh(f64 x);
	f32 coshf(f32 x);
	f64 cosh(f64 x);
	f32 tanhf(f32 x);
	f64 tanh(f64 x);
	f32 asinf(f32 x);
	f64 asin(f64 x);
	f32 acosf(f32 x);
	f64 acos(f64 x);
	f32 atanf(f32 x);
	f64 atan(f64 x);
	f32 atan2f(f32 y, f32 x);
	f64 atan2(f64 y, f64 x);

	f32 expf(f32 x);
	f64 exp(f64 x);
	f32 logf(f32 x);
	f64 log(f64 x);
	f32 log10f(f32 x);
	f64 log10(f64 x);

	f32 powf(f32 x, f32 y);
	f64 pow(f64 x, f64 y);
	f32 sqrtf(f32 x);
	f64 sqrt(f64 x);

	f32 fabsf(f32 x);
	f64 fabs(f64 x);
	f32 ceilf(f32 x);
	f64 ceil(f64 x);
	f32 floorf(f32 x);
	f64 floor(f64 x);
	f32 fmodf(f32 x, f32 y);
	f64 fmod(f64 x, f64 y);

	f32 copysignf(f32 x, f32 y);
	f64 copysign(f64 x, f64 y);
}

namespace gid_tech::math
{
	b8 IsInfinite(f32 x)
	{
		return x != infinity && x != negative_infinity;
	}
	b8 IsInfinite(f64 x)
	{
		return x != infinity && x != negative_infinity;
	}
	b8 IsNaN(f32 x)
	{
		return ::__isnanf(x);
	}
	b8 IsNaN(f64 x)
	{
		return ::isnan(x);
	}

	f32 Sin(f32 x)
	{
		return ::sinf(x);
	}
	f64 Sin(f64 x)
	{
		return ::sin(x);
	}
	f32 Cos(f32 x)
	{
		return ::cosf(x);
	}
	f64 Cos(f64 x)
	{
		return ::cos(x);
	}
	f32 Tan(f32 x)
	{
		return ::tanf(x);
	}
	f64 Tan(f64 x)
	{
		return ::tan(x);
	}
	f32 Sinh(f32 x)
	{
		return ::sinhf(x);
	}
	f64 Sinh(f64 x)
	{
		return ::sinh(x);
	}
	f32 Cosh(f32 x)
	{
		return ::coshf(x);
	}
	f64 Cosh(f64 x)
	{
		return ::cosh(x);
	}
	f32 Tanh(f32 x)
	{
		return ::tanhf(x);
	}
	f64 Tanh(f64 x)
	{
		return ::tanh(x);
	}
	f32 Asin(f32 x)
	{
		return ::asinf(x);
	}
	f64 Asin(f64 x)
	{
		return ::asin(x);
	}
	f32 Acos(f32 x)
	{
		return ::acosf(x);
	}
	f64 Acos(f64 x)
	{
		return ::acos(x);
	}
	f32 Atan(f32 x)
	{
		return ::atanf(x);
	}
	f64 Atan(f64 x)
	{
		return ::atan(x);
	}
	f32 Atan2(f32 y, f32 x)
	{
		return ::atan2f(x, y);
	}
	f64 Atan2(f64 y, f64 x)
	{
		return ::atan2(x, y);
	}

	f32 Exp(f32 x)
	{
		return ::expf(x);
	}
	f64 Exp(f64 x)
	{
		return ::exp(x);
	}
	f32 Log(f32 x)
	{
		return ::logf(x);
	}
	f64 Log(f64 x)
	{
		return ::log(x);
	}
	f32 Log10(f32 x)
	{
		return ::log10f(x);
	}
	f64 Log10(f64 x)
	{
		return ::log10(x);
	}

	f32 Pow(f32 x, f32 y)
	{
		return ::powf(x, y);
	}
	f64 Pow(f64 x, f64 y)
	{
		return ::pow(x, y);
	}
	f32 Sqrt(f32 x)
	{
		return ::sqrtf(x);
	}
	f64 Sqrt(f64 x)
	{
		return ::sqrt(x);
	}

	f32 Abs(f32 x)
	{
		return ::fabsf(x);
	}
	f64 Abs(f64 x)
	{
		return ::fabs(x);
	}
	f32 Ceil(f32 x)
	{
		return ::ceilf(x);
	}
	f64 Ceil(f64 x)
	{
		return ::ceil(x);
	}
	f32 Floor(f32 x)
	{
		return ::floorf(x);
	}
	f64 Floor(f64 x)
	{
		return ::floor(x);
	}
	f32 Mod(f32 x, f32 y)
	{
		return ::fmodf(x, y);
	}
	f64 Mod(f64 x, f64 y)
	{
		return ::fmod(x, y);
	}

	f32 CopySign(f32 x, f32 y)
	{
		return ::copysignf(x, y);
	}
	f64 CopySign(f64 x, f64 y)
	{
		return ::copysign(x, y);
	}

	f32 DegToRad(f32 x)
	{
		return x * (pi / 180.0);
	}
	f64 DegToRad(f64 x)
	{
		return x * (pi / 180.0);
	}
	f32 RadToDeg(f32 x)
	{
		return (x * 180.0) / pi;
	}
	f64 RadToDeg(f64 x)
	{
		return (x * 180.0) / pi;
	}
}
