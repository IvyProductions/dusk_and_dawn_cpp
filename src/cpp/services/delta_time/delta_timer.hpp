#pragma once

#include <collections/array.hpp>
#include <types/types.hpp>

using namespace gid_tech::types;
using namespace gid_tech::collections;

namespace gid_tech::delta_time
{
	class DeltaTimer
	{
	private:
		usize last_index{};
		Array<f64, 3> time_points{};

	public:
		DeltaTimer();
		f32 Step();
	};
}
