#pragma once

#ifdef WINDOWS

#include "../window.hpp"
#include "implementation/window.hpp"

namespace gid_tech::windowing
{
	struct Window::Implementation
	{
		win32::Window window;
	};
}

#endif
