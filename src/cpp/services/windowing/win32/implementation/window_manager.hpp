#pragma once

#ifdef WINDOWS

#include <string>

#include <Windows.h>
#undef CreateWindow
#undef GetClassName

#include "../../input.hpp"

using namespace std;

namespace gid_tech::windowing::win32
{
	class WindowManager
	{
	private:
		HINSTANCE const hinstance;
		wstring const class_name;

	public:
		WindowManager(wstring class_name);
		~WindowManager();

		HINSTANCE GetHinstance() const;
		wstring const & GetClassName() const;

	private:
		static LRESULT WindowProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam);
	};
}

#endif
