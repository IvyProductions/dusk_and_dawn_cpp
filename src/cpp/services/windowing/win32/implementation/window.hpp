#pragma once

#ifdef WINDOWS

#include <string>

#include <Windows.h>
#undef CreateWindow

#include <aggregates/optional.hpp>
#include <types/types.hpp>

#include "../../input.hpp"
#include "../../size.hpp"

using namespace std;
using namespace gid_tech::types;
using namespace gid_tech::aggregates;

namespace gid_tech::windowing::win32
{
	class WindowManager;

	class Window
	{
	private:
		WindowManager const & window_manager;

		HWND const hwnd;
		string const title;
		Size size;
		Input input;

	public:
		Window(WindowManager const & window_manager, string title, Size size);
		~Window();

		Window(Window const &) = delete;
		Window & operator=(Window const &) = delete;

		HWND GetHWND() const;
		Input const & GetInput() const;
		string const & GetTitle() const;
		Size const & GetSize() const;

		void Tick();

		LRESULT WindowProc(UINT msg, WPARAM wparam, LPARAM lparam);

	private:
		static HWND CreateHWND(HINSTANCE hinstance, wstring const & class_name, string const & title, Size size);
		static Optional<Input::Keyboard::Key> ToKey(WPARAM wparam);
	};
}

#endif
