#ifdef WINDOWS

#include "window.hpp"

#include "implementation/window.hpp"
#include "window_manager.hpp"

namespace gid_tech::windowing
{
	Window::Window(WindowManager const & window_manager, string title, Size size) :
		window_manager(window_manager),
		implementation(*reinterpret_cast<Implementation *>(const_cast<usize *>(implementation_data.elements)))
	{
		static_assert(sizeof(Window::implementation_data) >= sizeof(Implementation));
		new (&implementation) Implementation {
			.window = win32::Window(window_manager.GetImplementation().window_manager, move(title), size),
		};
	}

	Window::~Window()
	{
		implementation.~Implementation();
	}

	Input const & Window::GetInput() const
	{
		return implementation.window.GetInput();
	}

	string const & Window::GetTitle() const
	{
		return implementation.window.GetTitle();
	}

	Size const & Window::GetSize() const
	{
		return implementation.window.GetSize();
	}

	void Window::Tick()
	{
		implementation.window.Tick();
	}

	void * Window::GetImplementationHandle() const
	{
		return implementation.window.GetHWND();
	}
}

#endif
