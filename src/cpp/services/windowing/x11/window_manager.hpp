#pragma once

#ifdef LINUX

#include "../window_manager.hpp"
#include "implementation/window_manager.hpp"

namespace gid_tech::windowing
{
	struct WindowManager::Implementation
	{
		x11::WindowManager window_manager;
	};
}

#endif
