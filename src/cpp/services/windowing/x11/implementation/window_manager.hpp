#pragma once

#ifdef LINUX

#include <X11/Xlib.h>

namespace gid_tech::windowing::x11
{
	class WindowManager
	{
	private:
		::Display * const display;

	public:
		WindowManager();
		~WindowManager();

		::Display* GetDisplay() const;
	};
}

#endif
