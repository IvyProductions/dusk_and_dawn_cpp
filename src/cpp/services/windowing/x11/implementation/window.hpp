#pragma once

#ifdef LINUX

#include <string>

#include <X11/Xlib.h>
#include <X11/keysym.h>

#include <aggregates/optional.hpp>
#include <types/types.hpp>

#include "../../input.hpp"
#include "../../size.hpp"

using namespace std;
using namespace gid_tech::aggregates;

namespace gid_tech::windowing::x11
{
	class WindowManager;

	class Window
	{
	private:
		WindowManager const & window_manager;

		::Window const window;
		::Atom wm_delete_window;
		string const title;
		Size size;
		Input input;

	public:
		Window(WindowManager const & window_manager, string title, Size size);
		~Window();

		Window(Window const &) = delete;
		Window & operator=(Window const &) = delete;

		::Window GetWindow() const;
		Input const & GetInput() const;
		string const & GetTitle() const;
		Size const & GetSize() const;

		void Tick();

	private:
		static ::Window CreateWindow(::Display * display, string const & title, Size size);
		static ::Atom CreateAtom(::Display * display, ::Window window);
		static Optional<Input::Keyboard::Key> ToKey(::KeyCode key_code);
		static Optional<Input::Mouse::Button> ToButton(u32 button);
	};
}

#endif
