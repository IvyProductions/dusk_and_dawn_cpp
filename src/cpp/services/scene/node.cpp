#include "node.hpp"

#include <algorithm>
#include <utility>

#include <algebra/transform.hpp>

#include "scene.hpp"

namespace gid_tech::scene
{
	Node::Node(string name, Transform transform) :
		Node(move(name), transform, OptionalNone)
	{
	}

	Node::Node(string name, Transform transform, Scene * scene) :
		Node(move(name), transform, Optional<variant<Scene *, Node *>>(scene))
	{
	}

	Node::Node(string name, Transform transform, Node * parent) :
		Node(move(name), transform, Optional<variant<Scene *, Node *>>(parent))
	{
	}

	Node::Node(string name, Transform transform, Optional<variant<Scene *, Node *>> scene_or_parent) :
		name(move(name)),
		scene_or_parent(OptionalNone),
		transform(transform)
	{
		SetSceneOrParent(scene_or_parent);
	}

	Node::~Node()
	{
		for (auto * c : children)
			c->SetSceneOrParent(scene_or_parent);
		SetSceneOrParent(OptionalNone);
	}

	string const & Node::GetName() const
	{
		return name;
	}

	Optional<Scene *> Node::GetScene()
	{
		if (scene_or_parent.IsSome())
		{
			if (auto * scene = get_if<Scene *>(&scene_or_parent.Value()))
				return Optional<Scene *>(*scene);

			if (auto * parent = get_if<Node *>(&scene_or_parent.Value()))
				return (*parent)->GetScene();
		}
		return OptionalNone;
	}

	Optional<Scene const *> Node::GetScene() const
	{
		if (scene_or_parent.IsSome())
		{
			if (auto * scene = get_if<Scene *>(&scene_or_parent.Value()))
				return Optional<Scene const *>(*scene);
			if (auto * parent = get_if<Node *>(&scene_or_parent.Value()))
				return ((Node const *) *parent)->GetScene();
		}
		return OptionalNone;
	}

	Optional<Node *> Node::GetParent()
	{
		if (scene_or_parent.IsSome())
		{
			if (auto * parent = get_if<Node *>(&scene_or_parent.Value()))
				return Optional<Node *>(*parent);
		}
		return OptionalNone;
	}

	Optional<Node const *> Node::GetParent() const
	{
		if (scene_or_parent.IsSome())
		{
			if (auto * parent = get_if<Node *>(&scene_or_parent.Value()))
				return Optional<Node const *>(*parent);
		}
		return OptionalNone;
	}

	void Node::SetScene(Scene * scene)
	{
		SetSceneOrParent(Optional<variant<Scene *, Node *>>(scene));
	}

	void Node::SetParent(Node * parent)
	{
		SetSceneOrParent(Optional<variant<Scene *, Node *>>(parent));
	}

	void Node::SetSceneOrParent(Optional<variant<Scene *, Node *>> scene_or_parent)
	{
		if (this->scene_or_parent == scene_or_parent)
			return;

		if (auto scene = GetScene())
			scene.Value()->RemoveNode(this);
		if (auto parent = GetParent())
			parent.Value()->RemoveChild(this);

		this->scene_or_parent = scene_or_parent;

		if (auto scene = GetScene())
			scene.Value()->AddNode(this);
		if (auto parent = GetParent())
			parent.Value()->AddChild(this);

		TransformChanged();
		SceneChanged();
	}

	usize Node::GetChildCount()
	{
		return children.size();
	}

	Node * Node::GetChild(usize index)
	{
		return children[index];
	}

	Node * const Node::GetChild(usize index) const
	{
		return children[index];
	}

	Optional<Node *> Node::FindChild(string name)
	{
		for (auto * c : children)
		{
			if (c->name == name)
				return Optional<Node *>(c);
		}
		return OptionalNone;
	}

	Optional<Node const *> Node::FindChild(string name) const
	{
		for (auto * c : children)
		{
			if (c->name == name)
				return Optional<Node const *>(c);
		}
		return OptionalNone;
	}

	Node::Transform const & Node::GetTransform() const
	{
		return transform;
	}

	void Node::SetTransform(Transform transform)
	{
		if (this->transform.translation != transform.translation && this->transform.rotation != transform.rotation && this->transform.scale != transform.scale)
		{
			this->transform = transform;
			TransformChanged();
		}
	}

	Vector<f32, 3> const & Node::GetPosition() const
	{
		return transform.translation;
	}

	void Node::SetPosition(Vector<f32, 3> position)
	{
		if (transform.translation != position)
		{
			transform.translation = position;
			TransformChanged();
		}
	}

	Quaternion<f32> const & Node::GetRotation() const
	{
		return transform.rotation;
	}

	void Node::SetRotation(Quaternion<f32> rotation)
	{
		if (transform.rotation != rotation)
		{
			transform.rotation = rotation;
			TransformChanged();
		}
	}

	Vector<f32, 3> const & Node::GetScale() const
	{
		return transform.scale;
	}

	void Node::SetScale(Vector<f32, 3> scale)
	{
		if (transform.scale != scale)
		{
			transform.scale = scale;
			TransformChanged();
		}
	}

	Vector<f32, 3> Node::GetWorldPosition() const
	{
		auto local_to_world = GetLocalToWorld();
		return {local_to_world[3][0], local_to_world[3][1], local_to_world[3][2]};
	}

	void Node::SetWorldPosition(Vector<f32, 3> position)
	{
		if (auto parent = GetParent())
		{
			auto local_position = parent.Value()->GetWorldToLocal() * Vector<f32, 4>{position.x, position.y, position.z, 1};
			SetPosition(Vector<f32, 3>{local_position.x, local_position.y, local_position.z});
		}
		else
			SetPosition(position);
	}

	Quaternion<f32> Node::GetWorldRotation() const
	{
		auto local_to_world = GetLocalToWorld();
		return algebra::Transform<f32>::MatrixToRotation(local_to_world);
	}

	void Node::SetWorldRotation(Quaternion<f32> rotation)
	{
		if (auto parent = GetParent())
		{
			auto local_rotaion = parent.Value()->GetWorldToLocal() * algebra::Transform<f32>::Rotation(rotation);
			SetRotation(algebra::Transform<f32>::MatrixToRotation(local_rotaion));
		}
		else
			SetRotation(rotation);
	}

	void Node::Translate(Vector<f32, 3> delta)
	{
		SetPosition(GetPosition() + delta);
	}

	void Node::Rotate(Quaternion<f32> delta)
	{
		SetRotation(GetRotation() * delta);
	}

	void Node::Scale(Vector<f32, 3> delta)
	{
		SetScale(GetScale() + delta);
	}

	void Node::TranslateWorld(Vector<f32, 3> delta)
	{
		SetWorldPosition(GetWorldPosition() + delta);
	}

	void Node::RotateWorld(Quaternion<f32> delta)
	{
		SetWorldRotation(GetWorldRotation() * delta);
	}

	Vector<f32, 3> Node::GetWorldForward() const
	{
		auto local_to_world = GetLocalToWorld();
		return {local_to_world[2][0], local_to_world[2][1], local_to_world[2][2]};
	}

	Vector<f32, 3> Node::GetWorldRight() const
	{
		auto local_to_world = GetLocalToWorld();
		return {local_to_world[0][0], local_to_world[0][1], local_to_world[0][2]};
	}

	Vector<f32, 3> Node::GetWorldUp() const
	{
		auto local_to_world = GetLocalToWorld();
		return {local_to_world[1][0], local_to_world[1][1], local_to_world[1][2]};
	}

	Matrix<f32, 4, 4> const & Node::GetLocalToWorld() const
	{
		if (!local_to_world.IsSome())
		{
			if (auto parent = GetParent())
				local_to_world = Optional<Matrix<f32, 4, 4>>(parent.Value()->GetLocalToWorld() * algebra::Transform<f32>::TRS(transform.translation, transform.rotation, transform.scale));
			else
				local_to_world = Optional<Matrix<f32, 4, 4>>(algebra::Transform<f32>::TRS(transform.translation, transform.rotation, transform.scale));
		}
		return local_to_world.Value();
	}

	Matrix<f32, 4, 4> const & Node::GetWorldToLocal() const
	{
		if (!world_to_local.IsSome())
			world_to_local = Optional<Matrix<f32, 4, 4>>(GetLocalToWorld().Inverse());
		return world_to_local.Value();
	}

	Node * Node::Copy(function<void(Node *)> node_alloc_callback, Optional<variant<Scene *, Node *>> scene_or_parent)
	{
		auto node = new Node(name, transform, scene_or_parent);
		node_alloc_callback(node);
		for (auto c : children)
			c->Copy(node_alloc_callback, Optional<variant<Scene *, Node *>>(node));
		return node;
	}

	tuple<Node *, vector<shared_ptr<Node>>> Node::MakeCopy(Optional<variant<Scene *, Node *>> scene_or_parent)
	{
		vector<shared_ptr<Node>> nodes;
		auto node = Copy([&](Node * n) { nodes.emplace_back(n); }, scene_or_parent);
		return make_tuple(node, nodes);
	}

	Node * Node::Copy(function<void(Node *)> node_alloc_callback, optional<variant<Scene *, Node *>> scene_or_parent)
	{
		auto node = new Node(name, transform, scene_or_parent);
		node_alloc_callback(node);
		for (auto c : children)
			c->Copy(node_alloc_callback, make_optional(node));
		return node;
	}

	tuple<Node *, vector<shared_ptr<Node>>> Node::MakeCopy(optional<variant<Scene *, Node *>> scene_or_parent)
	{
		vector<shared_ptr<Node>> nodes;
		auto node = Copy([&](Node * n) { nodes.emplace_back(n); }, scene_or_parent);
		return make_tuple(node, nodes);
	}

	void Node::TransformChanged()
	{
		local_to_world = OptionalNone;
		world_to_local = OptionalNone;
		for (auto & child : children)
			child->TransformChanged();
	}

	void Node::SceneChanged()
	{
	}

	void Node::AddChild(Node * node)
	{
		children.emplace_back(node);
	}

	void Node::RemoveChild(Node * node)
	{
		children.erase(find(children.begin(), children.end(), node));
	}
}
