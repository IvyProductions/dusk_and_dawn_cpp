#include "light_node.hpp"

#include "scene.hpp"

namespace gid_tech::scene
{
	LightNode::LightNode(string name, Transform transform, Config config) :
		LightNode(move(name), transform, OptionalNone, config)
	{
	}

	LightNode::LightNode(string name, Transform transform, Scene * scene, Config config) :
		LightNode(move(name), transform, Optional<variant<Scene *, Node *>>(scene), config)
	{
	}

	LightNode::LightNode(string name, Transform transform, Node * parent, Config config) :
		LightNode(move(name), transform, Optional<variant<Scene *, Node *>>(parent), config)
	{
	}

	LightNode::LightNode(string name, Transform transform, Optional<variant<Scene *, Node *>> scene_or_parent, Config config) :
		Node(move(name), transform, OptionalNone),
		config(config),
		light(nullptr)
	{
		SetSceneOrParent(scene_or_parent);
	}

	LightNode::~LightNode()
	{
		SetSceneOrParent(OptionalNone);
	}

	BoundingBox const & LightNode::GetBoundingBox()
	{
		if (bounding_box.IsNone())
		{
			switch (config.type)
			{
			case Config::Type::Ambient:
			case Config::Type::Directional:
				bounding_box = Optional<BoundingBox>(BoundingBox{{-INFINITY, -INFINITY, -INFINITY}, {INFINITY, INFINITY, INFINITY}});
				break;
			case Config::Type::Point:
			case Config::Type::Spot:
			{
				f32 t = 0.1;
				auto intensity = config.color.r + config.color.g + config.color.b;
				auto x = t / intensity;
				auto dist = sqrt((1 - x) / x);

				bounding_box = Optional<BoundingBox>(BoundingBox{{-dist, -dist, -dist}, {dist, dist, dist}});
				auto pos = GetWorldPosition();
				bounding_box->min += pos;
				bounding_box->max += pos;
				break;
			}
			}
		}
		return bounding_box.Value();
	}

	Node * LightNode::Copy(function<void(Node *)> node_alloc_callback, Optional<variant<Scene *, Node *>> scene_or_parent)
	{
		auto node = new LightNode(GetName(), GetTransform(), scene_or_parent, config);
		node_alloc_callback(node);
		for (usize i = 0; i < GetChildCount(); i++)
			GetChild(i)->Copy(node_alloc_callback, Optional<variant<Scene *, Node *>>(static_cast<Node *>(node)));
		return node;
	}

	void LightNode::TransformChanged()
	{
		Node::TransformChanged();
		bounding_box = OptionalNone;
		needs_upload = true;
	}

	void LightNode::SceneChanged()
	{
		Node::SceneChanged();
		light.reset();
	}

	data::Light LightNode::GetLightData()
	{
		return data::Light{
			.world_to_local = GetWorldToLocal(),
			.local_to_world = GetLocalToWorld(),
			.color = data::Color{config.color.r, config.color.g, config.color.b, 0},
			.type = (int32_t) config.type,
			.cutoff_inner = config.cutoff.inner,
			.cutoff_outer = config.cutoff.outer,
		};
	}

	void LightNode::Upload()
	{
		if (light == nullptr)
			light = make_unique<Light>(GetScene().Value()->scene, GetLightData());
		else if (needs_upload)
			light->Set(GetLightData());
		needs_upload = false;
	}
}
