#pragma once

#include <functional>
#include <memory>
#include <string>
#include <variant>
#include <vector>

#include <aggregates/optional.hpp>
#include <algebra/matrix.hpp>
#include <algebra/quaternion.hpp>
#include <algebra/vector.hpp>

using namespace std;
using namespace gid_tech::algebra;
using namespace gid_tech::aggregates;

namespace gid_tech::scene
{
	class Scene;

	class Node
	{
		friend class Scene;

	public:
		struct Transform
		{
			Vector<f32, 3> translation{0, 0, 0};
			Quaternion<f32> rotation{Quaternion<f32>::Identity()};
			Vector<f32, 3> scale{1, 1, 1};
		};

	private:
		string name;

		Optional<variant<Scene *, Node *>> scene_or_parent;
		vector<Node *> children;

		Transform transform;

		Optional<Matrix<f32, 4, 4>> mutable local_to_world;
		Optional<Matrix<f32, 4, 4>> mutable world_to_local;

	public:
		Node(string name, Transform transform);
		Node(string name, Transform transform, Scene * scene);
		Node(string name, Transform transform, Node * parent);
		Node(string name, Transform transform, Optional<variant<Scene *, Node *>> scene_or_parent);
		virtual ~Node();

		Node(Node const &) = delete;
		Node operator=(Node const &) = delete;

		string const & GetName() const;

		Optional<Scene *> GetScene();
		Optional<Scene const *> GetScene() const;

		Optional<Node *> GetParent();
		Optional<Node const *> GetParent() const;

		void SetScene(Scene * scene);
		void SetParent(Node * parent);
		void SetSceneOrParent(Optional<variant<Scene *, Node *>> scene_or_parent);

		usize GetChildCount();
		Node * GetChild(usize index);
		Node * const GetChild(usize index) const;

		Optional<Node *> FindChild(string name);
		Optional<Node const *> FindChild(string name) const;

		Transform const & GetTransform() const;
		void SetTransform(Transform transform);

		Vector<f32, 3> const & GetPosition() const;
		void SetPosition(Vector<f32, 3> position);

		Quaternion<f32> const & GetRotation() const;
		void SetRotation(Quaternion<f32> rotation);

		Vector<f32, 3> const & GetScale() const;
		void SetScale(Vector<f32, 3> scale);

		Vector<f32, 3> GetWorldPosition() const;
		void SetWorldPosition(Vector<f32, 3> position);

		Quaternion<f32> GetWorldRotation() const;
		void SetWorldRotation(Quaternion<f32> rotation);

		void Translate(Vector<f32, 3> delta);
		void Rotate(Quaternion<f32> delta);
		void Scale(Vector<f32, 3> delta);

		void TranslateWorld(Vector<f32, 3> delta);
		void RotateWorld(Quaternion<f32> delta);

		Vector<f32, 3> GetWorldForward() const;
		Vector<f32, 3> GetWorldRight() const;
		Vector<f32, 3> GetWorldUp() const;


		Matrix<f32, 4, 4> const & GetLocalToWorld() const;
		Matrix<f32, 4, 4> const & GetWorldToLocal() const;

		virtual Node * Copy(function<void(Node *)> node_alloc_callback, Optional<variant<Scene *, Node *>> scene_or_parent);
		tuple<Node *, vector<shared_ptr<Node>>> MakeCopy(Optional<variant<Scene *, Node *>> scene_or_parent);

	protected:
		virtual void TransformChanged();
		virtual void SceneChanged();

	private:
		void AddChild(Node * node);
		void RemoveChild(Node * node);

	public:
		static Optional<variant<Scene *, Node *>> MakeSceneOrParent();
		static Optional<variant<Scene *, Node *>> MakeSceneOrParent(Scene *);
		static Optional<variant<Scene *, Node *>> MakeSceneOrParent(Node *);
	};
}
