#pragma once

#include <memory>

#include <algebra/transform.hpp>
#include <bounds/bounding_box.hpp>
#include <rendering/scene/light.hpp>

#include "node.hpp"

namespace gid_tech::scene
{
	using namespace std;
	using namespace bounds;
	using namespace rendering;

	class Scene;

	class LightNode : public Node
	{
		friend class Scene;

	public:
		struct Config
		{
			enum class Type : u32
			{
				Ambient = 0,
				Directional = 1,
				Point = 2,
				Spot = 3,
			};
			struct Color
			{
				f32 r{1}, g{1}, b{1};
			};
			struct CutOff
			{
				f32 inner{algebra::Transform<f32>::ToRadians(10)};
				f32 outer{algebra::Transform<f32>::ToRadians(30)};
			};

			Type type{};
			Color color{};
			CutOff cutoff;
		};

	private:
		Config config;

		unique_ptr<Light> light;
		// Optional<shared_ptr<Shadow>> shadow;

		Optional<BoundingBox> bounding_box;
		bool needs_upload{false};

	public:
		LightNode(string name, Transform transform, Config config);
		LightNode(string name, Transform transform, Scene * scene, Config config);
		LightNode(string name, Transform transform, Node * parent, Config config);
		LightNode(string name, Transform transform, Optional<variant<Scene *, Node *>> scene_or_parent, Config config);
		virtual ~LightNode();


		BoundingBox const & GetBoundingBox();

		Node * Copy(function<void(Node *)> node_alloc_callback, Optional<variant<Scene *, Node *>> scene_or_parent) override;

	protected:
		void TransformChanged() override;
		void SceneChanged() override;

	private:
		data::Light GetLightData();
		void Upload();
	};
}
