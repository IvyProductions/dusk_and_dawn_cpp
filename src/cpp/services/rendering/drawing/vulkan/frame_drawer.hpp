#pragma once

#include <vector>

#include <vulkan/vulkan.h>

#include "../../resources/vulkan/image.hpp"
#include "../../resources/vulkan/material.hpp"
#include "../../resources/vulkan/mesh.hpp"
#include "../../resources/vulkan/pipeline.hpp"
#include "../../scene/vulkan/camera.hpp"
#include "../../scene/vulkan/light.hpp"
#include "../../scene/vulkan/scene.hpp"
#include "../../scene/vulkan/surface.hpp"
#include "../../targets/vulkan/frame.hpp"

#include "../../vulkan/device.hpp"
#include "../../vulkan/draw_configs/frame_config.hpp"

namespace gid_tech::rendering::implementation
{
	using std::vector;

	struct FrameDraw
	{
		vector<Light const *> lights;
		Surface const * surface;
		Camera const * camera;

		Pipeline const * pipeline;
		Mesh const * mesh;
		Material const * material;
		vector<Image const *> images;
	};

	class FrameDrawer
	{
	private:
		Device & device;
		FrameConfig const & frame_config;

		VkCommandPool const command_pool;
		VkDescriptorPool const descriptor_pool; // todo pool pool
		VkFence const fence;

	public:
		FrameDrawer(Device & device, FrameConfig const & frame_config);
		~FrameDrawer();

		FrameDrawer(FrameDrawer const &);
		FrameDrawer & operator=(FrameDrawer const &);


		void Draw(SceneManager const * scene, vector<FrameDraw> const & draws, Frame const * frame);

	private:
		static VkCommandPool CreateCommandPool(VkDevice device, u32 queue_index);
		static VkDescriptorPool CreateDescriptorPool(VkDevice device);
		static VkFence CreateFence(VkDevice device);
	};
}
