#pragma once

#include <vector>

#include <hidden/hidden.hpp>

#include "../resources/image.hpp"
#include "../resources/material.hpp"
#include "../resources/mesh.hpp"
#include "../resources/pipeline.hpp"
#include "../scene/camera.hpp"
#include "../scene/light.hpp"
#include "../scene/scene.hpp"
#include "../scene/surface.hpp"
#include "../targets/frame.hpp"

#include "../render_manager.hpp"

namespace gid_tech::rendering::implementation
{
	class FrameDrawer;
}

namespace gid_tech::rendering
{
	using std::vector;

	using FrameDrawerImplementation = Hidden<implementation::FrameDrawer, 40>;

	struct FrameDraw
	{
		vector<Light const *> lights;
		Surface const * surface;
		Camera const * camera;

		Pipeline const * pipeline;
		Mesh const * mesh;
		Material const * material;
		vector<Image const *> images;
	};

	class FrameDrawer : public FrameDrawerImplementation
	{
	public:
		FrameDrawer(RenderManager & render_manager);
		~FrameDrawer();

		void Draw(SceneManager const * scene, vector<FrameDraw> const & draws, Frame const * frame);
	};
}
