#include "pipeline.hpp"

#include <collections/array.hpp>

#include "../../vulkan/render_manager.hpp"
#include "../pipeline.hpp"

using namespace gid_tech::collections;

namespace gid_tech::rendering
{
	using std::move;

	Pipeline::Pipeline(RenderManager & render_manager, u32 image_count, bool is_transparent, vector<u8> vert_data, vector<u8> frag_data) :
		PipelineImplementation(render_manager.device, render_manager.frame_config, image_count, is_transparent, move(vert_data), move(frag_data))
	{
	}

	Pipeline::~Pipeline()
	{
	}

	u32 Pipeline::GetImageCount() const
	{
		return PipelineImplementation::GetImageCount();
	}

	bool Pipeline::IsTransparent() const
	{
		return PipelineImplementation::IsTransparent();
	}
}

namespace gid_tech::rendering::implementation
{
	Pipeline::Pipeline(Device & device, FrameConfig const & frame_config, u32 image_count, bool is_transparent, vector<u8> vert_data, vector<u8> frag_data) :
		device(device),
		image_count(image_count),
		is_transparent(is_transparent),
		instance_descriptor_set_layout(CreateInstanceDescriptorSetLayout(device.GetDevice(), image_count)),
		pipeline_layout(CreatePipelineLayout(device.GetDevice(), frame_config.GetCommonDescriptorSetLayout(), instance_descriptor_set_layout)),
		pipeline(CreatePipeline(device.GetDevice(), frame_config.GetRenderPass(), pipeline_layout, is_transparent, vert_data, frag_data))
	{
	}

	Pipeline::~Pipeline()
	{
		vkDestroyDescriptorSetLayout(device.GetDevice(), instance_descriptor_set_layout, nullptr);
		vkDestroyPipelineLayout(device.GetDevice(), pipeline_layout, nullptr);
		vkDestroyPipeline(device.GetDevice(), pipeline, nullptr);
	}

	u32 Pipeline::GetImageCount() const
	{
		return image_count;
	}

	bool Pipeline::IsTransparent() const
	{
		return is_transparent;
	}

	VkDescriptorSetLayout Pipeline::GetInstanceDescriptorSetLayout() const
	{
		return instance_descriptor_set_layout;
	}

	VkPipelineLayout Pipeline::GetPipelineLayout() const
	{
		return pipeline_layout;
	}

	VkPipeline Pipeline::GetPipeline() const
	{
		return pipeline;
	}

	VkDescriptorSetLayout Pipeline::CreateInstanceDescriptorSetLayout(VkDevice device, u32 image_count)
	{
		Array<VkDescriptorSetLayoutBinding, 2> bindings{
			// material
			VkDescriptorSetLayoutBinding{
				.binding = 10,
				.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
				.descriptorCount = 1,
				.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
			},
			// textures
			VkDescriptorSetLayoutBinding{
				.binding = 11,
				.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
				.descriptorCount = image_count,
				.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
			},
		};

		VkDescriptorSetLayoutCreateInfo create_info{
			.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
			.bindingCount = bindings.Count(),
			.pBindings = bindings.elements,
		};

		VkDescriptorSetLayout descriptor_set_layout{};
		vkCreateDescriptorSetLayout(device, &create_info, nullptr, &descriptor_set_layout);
		return descriptor_set_layout;
	}

	VkPipelineLayout Pipeline::CreatePipelineLayout(VkDevice device, VkDescriptorSetLayout common_descriptor_set_layout, VkDescriptorSetLayout instance_descriptor_set_layout)
	{
		Array<VkPushConstantRange, 1> push_constants{
			VkPushConstantRange{
				.stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
				.offset = 0,
				.size = sizeof(u32) * 7,
			},
		};

		Array<VkDescriptorSetLayout, 2> descriptor_set_layouts{
			common_descriptor_set_layout,
			instance_descriptor_set_layout,
		};

		VkPipelineLayoutCreateInfo create_info{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
			.setLayoutCount = descriptor_set_layouts.Count(),
			.pSetLayouts = descriptor_set_layouts.elements,
			.pushConstantRangeCount = push_constants.Count(),
			.pPushConstantRanges = push_constants.elements,
		};

		VkPipelineLayout pipeline_layout{};
		vkCreatePipelineLayout(device, &create_info, nullptr, &pipeline_layout);
		return pipeline_layout;
	}

	VkPipeline Pipeline::CreatePipeline(
		VkDevice device,
		VkRenderPass render_pass,
		VkPipelineLayout pipeline_layout,
		bool is_transparent,
		vector<u8> const & vert_data,
		vector<u8> const & frag_data)
	{
		VkShaderModuleCreateInfo vert_shader_module_create_info{
			.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
			.codeSize = vert_data.size(),
			.pCode = reinterpret_cast<u32 const *>(vert_data.data()),
		};
		VkShaderModule vert_shader_module{};
		vkCreateShaderModule(device, &vert_shader_module_create_info, nullptr, &vert_shader_module);

		VkShaderModuleCreateInfo frag_shader_module_create_info{
			.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
			.codeSize = frag_data.size(),
			.pCode = reinterpret_cast<u32 const *>(frag_data.data()),
		};
		VkShaderModule frag_shader_module{};
		vkCreateShaderModule(device, &frag_shader_module_create_info, nullptr, &frag_shader_module);

		Array<VkPipelineShaderStageCreateInfo, 2> shader_stages{
			VkPipelineShaderStageCreateInfo{
				.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
				.stage = VK_SHADER_STAGE_VERTEX_BIT,
				.module = vert_shader_module,
				.pName = "main",
			},
			VkPipelineShaderStageCreateInfo{
				.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
				.stage = VK_SHADER_STAGE_FRAGMENT_BIT,
				.module = frag_shader_module,
				.pName = "main",
			},
		};

		Array<VkVertexInputBindingDescription, 1> vertex_binding_descriptions{
			VkVertexInputBindingDescription{
				.binding = 0,
				.stride = sizeof(f32) * 3 * 5 + sizeof(f32) * 2,
				.inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
			},
		};

		Array<VkVertexInputAttributeDescription, 6> vertex_attribute_descriptions{
			VkVertexInputAttributeDescription{
				.location = 0,
				.binding = 0,
				.format = VK_FORMAT_R32G32B32_SFLOAT,
				.offset = sizeof(f32) * 3 * 0,
			},
			VkVertexInputAttributeDescription{
				.location = 1,
				.binding = 0,
				.format = VK_FORMAT_R32G32B32_SFLOAT,
				.offset = sizeof(f32) * 3 * 1,
			},
			VkVertexInputAttributeDescription{
				.location = 2,
				.binding = 0,
				.format = VK_FORMAT_R32G32B32_SFLOAT,
				.offset = sizeof(f32) * 3 * 2,
			},
			VkVertexInputAttributeDescription{
				.location = 3,
				.binding = 0,
				.format = VK_FORMAT_R32G32B32_SFLOAT,
				.offset = sizeof(f32) * 3 * 3,
			},
			VkVertexInputAttributeDescription{
				.location = 4,
				.binding = 0,
				.format = VK_FORMAT_R32G32B32_SFLOAT,
				.offset = sizeof(f32) * 3 * 4,
			},
			VkVertexInputAttributeDescription{
				.location = 5,
				.binding = 0,
				.format = VK_FORMAT_R32G32_SFLOAT,
				.offset = sizeof(f32) * 3 * 5,
			},
		};

		VkPipelineVertexInputStateCreateInfo vertex_input_state{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
			.vertexBindingDescriptionCount = vertex_binding_descriptions.Count(),
			.pVertexBindingDescriptions = vertex_binding_descriptions.elements,
			.vertexAttributeDescriptionCount = vertex_attribute_descriptions.Count(),
			.pVertexAttributeDescriptions = vertex_attribute_descriptions.elements,
		};

		VkPipelineInputAssemblyStateCreateInfo input_assembly_state{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
			.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
		};

		Array<VkViewport, 1> view_ports{
			VkViewport{
				.x = 0.0,
				.y = 0.0,
				.width = 1.0,
				.height = 1.0,
				.minDepth = 0.0,
				.maxDepth = 1.0,
			},
		};

		Array<VkRect2D, 1> scissors{
			VkRect2D{
				.offset = VkOffset2D{.x = 0, .y = 0},
				.extent = VkExtent2D{.width = 1, .height = 1},
			},
		};

		VkPipelineViewportStateCreateInfo viewport_state{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
			.viewportCount = view_ports.Count(),
			.pViewports = view_ports.elements,
			.scissorCount = scissors.Count(),
			.pScissors = scissors.elements,
		};

		VkPipelineRasterizationStateCreateInfo rasterization_state{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
			.polygonMode = VK_POLYGON_MODE_FILL,
			.cullMode = VK_CULL_MODE_FRONT_BIT,
			.frontFace = VK_FRONT_FACE_CLOCKWISE,
			.lineWidth = 1.0,
		};

		VkPipelineMultisampleStateCreateInfo multisample_state{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
			.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
		};

		VkPipelineDepthStencilStateCreateInfo depth_stencil_state{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
			.depthTestEnable = VK_TRUE,
			.depthWriteEnable = is_transparent ? VkBool32{VK_FALSE} : VkBool32{VK_TRUE},
			.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL,
			.depthBoundsTestEnable = 0,
			.stencilTestEnable = 0,
			.front = VkStencilOpState{.compareOp = VK_COMPARE_OP_ALWAYS},
			.back = VkStencilOpState{.compareOp = VK_COMPARE_OP_ALWAYS},
			.minDepthBounds = 0.0,
			.maxDepthBounds = 1.0,
		};

		Array<VkPipelineColorBlendAttachmentState, 1> color_blend_attachments{
			VkPipelineColorBlendAttachmentState{
				.blendEnable = is_transparent ? VkBool32{VK_TRUE} : VkBool32{VK_FALSE},
				.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA,
				.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
				.colorBlendOp = VK_BLEND_OP_ADD,
				.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
				.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
				.alphaBlendOp = VK_BLEND_OP_ADD,
				.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
			},
		};

		VkPipelineColorBlendStateCreateInfo color_blend_state{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
			.attachmentCount = color_blend_attachments.Count(),
			.pAttachments = color_blend_attachments.elements,
		};

		Array<VkDynamicState, 2> dynamic_states{
			VK_DYNAMIC_STATE_VIEWPORT,
			VK_DYNAMIC_STATE_SCISSOR,
		};

		VkPipelineDynamicStateCreateInfo dynamic_state{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
			.dynamicStateCount = dynamic_states.Count(),
			.pDynamicStates = dynamic_states.elements,
		};

		VkGraphicsPipelineCreateInfo create_info{
			.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
			.stageCount = shader_stages.Count(),
			.pStages = shader_stages.elements,
			.pVertexInputState = &vertex_input_state,
			.pInputAssemblyState = &input_assembly_state,
			.pViewportState = &viewport_state,
			.pRasterizationState = &rasterization_state,
			.pMultisampleState = &multisample_state,
			.pDepthStencilState = &depth_stencil_state,
			.pColorBlendState = &color_blend_state,
			.pDynamicState = &dynamic_state,
			.layout = pipeline_layout,
			.renderPass = render_pass,
		};

		VkPipeline pipeline = {};
		vkCreateGraphicsPipelines(device, nullptr, 1, &create_info, nullptr, &pipeline);

		vkDestroyShaderModule(device, vert_shader_module, nullptr);
		vkDestroyShaderModule(device, frag_shader_module, nullptr);
		return pipeline;
	}
}
