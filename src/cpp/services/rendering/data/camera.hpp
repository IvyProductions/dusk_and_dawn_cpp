#pragma once

#include <algebra/matrix.hpp>

namespace gid_tech::rendering::data
{
	using algebra::Matrix;

	struct Camera
	{
		Matrix<f32, 4, 4> world_to_local;
		Matrix<f32, 4, 4> local_to_world;
		Matrix<f32, 4, 4> local_to_projection;
	};
}
