#pragma once

#include <algebra/vector.hpp>

namespace gid_tech::rendering::data
{
	using algebra::Vector;

	struct Vertex
	{
		Vector<f32, 3> position;
		Vector<f32, 3> normal;
		Vector<f32, 3> tangent;
		Vector<f32, 3> bitangent;
		Vector<f32, 3> color;
		Vector<f32, 2> uv;
	};
	
	struct Index
	{
		Vector<u32, 3> indices;
	};
}
