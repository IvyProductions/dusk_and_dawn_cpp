#pragma once

namespace gid_tech::rendering::data
{
	struct Color
	{
		f32 r{}, g{}, b{}, a{};
	};
}
