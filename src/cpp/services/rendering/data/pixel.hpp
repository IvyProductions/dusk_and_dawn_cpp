#pragma once

#include <types/types.hpp>

using namespace gid_tech::types;

namespace gid_tech::rendering::data
{
	struct Pixel
	{
		u8 r, g, b, a;
	};
}
