#pragma once

#include <algebra/matrix.hpp>

namespace gid_tech::rendering::data
{
	using algebra::Matrix;

	struct Surface
	{
		Matrix<f32, 4, 4> local_to_world;
	};
}
