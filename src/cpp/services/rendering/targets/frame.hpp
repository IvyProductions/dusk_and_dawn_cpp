#pragma once

#include <hidden/hidden.hpp>
#include <types/types.hpp>

#include "../render_manager.hpp"
#include "../size.hpp"

using namespace std;

namespace gid_tech::rendering::implementation
{
	class Frame;
}

namespace gid_tech::rendering
{
	using FrameImplementation = Hidden<implementation::Frame, 72>;

	class Frame : public FrameImplementation
	{
	public:
		Frame(RenderManager & render_manager, Size size);
		~Frame();

		Frame(Frame const &) = delete;
		Frame & operator=(Frame const &) = delete;


		Size GetSize() const;
		void SetSize(Size size);
	};
}
