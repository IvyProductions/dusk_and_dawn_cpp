#pragma once

#include "../data/surface.hpp"
#include "scene.hpp"

namespace gid_tech::rendering::implementation
{
	class Surface;
}

using namespace std;

namespace gid_tech::rendering
{
	using SurfaceImplementation = Hidden<implementation::Surface, 16>;

	class Surface : public SurfaceImplementation
	{
	public:
		Surface(SceneManager & scene, data::Surface const & data);
		~Surface();

		Surface(Surface const &) = delete;
		Surface & operator=(Surface const &) = delete;


		data::Surface const & Get() const;
		void Set(data::Surface const & data);
	};
}
