#pragma once

#include <vector>

#include <vulkan/vulkan.h>

#include "../../vulkan/device.hpp"
#include "../../vulkan/memory/memory_cache.hpp"
#include "../../vulkan/memory/memory_transfer.hpp"

using namespace std;

namespace gid_tech::rendering::implementation
{
	template<typename T>
	class ObjectArray
	{
	public:
		struct Index
		{
			u32 index;
			bool operator==(Index other)
			{
				return index == other.index;
			}
		};

	private:
		Device & device;
		MemoryCache & memory_cache;
		MemoryTransfer & memory_transfer;

		vector<Index> free;
		vector<T> data;

		VkBuffer buffer;
		VkDeviceMemory memory;

	public:
		ObjectArray(Device & device, MemoryCache & memory_cache, MemoryTransfer & memory_transfer, u32 initial_size);
		~ObjectArray();

		ObjectArray(ObjectArray const &) = delete;
		ObjectArray & operator=(ObjectArray const &) = delete;

	public:
		VkDescriptorBufferInfo GetDescriptorInfo() const;

		Index Add(T const & value);
		void Remove(Index index);

		T const & Get(Index index);
		void Set(Index index, T const & value);

	private:
		static VkBuffer CreateBuffer(VkDevice device, usize size);
		static VkMemoryRequirements GetMemoryRequirements(VkDevice device, VkBuffer buffer);
		static VkDeviceMemory CreateDeviceMemory(VkDevice device, VkBuffer buffer, u32 memory_type_index, VkDeviceSize size);
	};
}

#include "object_array.impl.hpp"
