#pragma once

#include "surface_array.hpp"

namespace gid_tech::rendering::implementation
{
	class Surface
	{
	public:
		SurfaceArray & surfaces;
		SurfaceArray::Index index;

	public:
		Surface(SurfaceArray & surfaces, data::Surface const & data);
		~Surface();


		data::Surface const & Get() const;
		void Set(data::Surface const & data);
	};
}
