#include "surface.hpp"
#include "scene.hpp"

#include "../scene.hpp"
#include "../surface.hpp"

namespace gid_tech::rendering
{
	Surface::Surface(SceneManager & scene, data::Surface const & data) :
		SurfaceImplementation(scene.surfaces, data)
	{
	}

	Surface::~Surface()
	{
	}

	data::Surface const & Surface::Get() const
	{
		return SurfaceImplementation::Get();
	}

	void Surface::Set(data::Surface const & data)
	{
		SurfaceImplementation::Set(data);
	}
}

namespace gid_tech::rendering::implementation
{
	Surface::Surface(SurfaceArray & surfaces, data::Surface const & data) :
		surfaces(surfaces),
		index(surfaces.Add(data))
	{
		Set(data);
	}

	Surface::~Surface()
	{
		surfaces.Remove(index);
	}

	data::Surface const & Surface::Get() const
	{
		return surfaces.Get(index);
	}

	void Surface::Set(data::Surface const & data)
	{
		surfaces.Set(index, data);
	}
}
