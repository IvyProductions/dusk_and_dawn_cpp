#pragma once

#include "../../vulkan/render_manager.hpp"

#include "camera_array.hpp"
#include "light_array.hpp"
#include "surface_array.hpp"

namespace gid_tech::rendering::implementation
{
	class SceneManager
	{
	public:
		LightArray lights;
		SurfaceArray surfaces;
		CameraArray cameras;

	public:
		SceneManager(RenderManager & render_manager);
	};
}
