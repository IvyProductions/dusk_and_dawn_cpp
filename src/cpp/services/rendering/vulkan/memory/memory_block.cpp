#include "memory_block.hpp"

#include <algorithm>

using namespace std;

namespace gid_tech::rendering::implementation
{
	MemoryBlock::MemoryBlock(Device & device, u32 memory_type_index, usize size) :
		device(device),
		memory_type_index(memory_type_index),
		size(size),
		memory(CreateDeviceMemory(device.GetDevice(), memory_type_index, size))
	{
	}

	MemoryBlock::~MemoryBlock()
	{
		vkFreeMemory(device.GetDevice(), memory, nullptr);
	}

	u32 MemoryBlock::GetMemoryTypeIndex() const
	{
		return memory_type_index;
	}

	bool MemoryBlock::IsEmpty() const
	{
		return used_block_regions.empty();
	}

	bool MemoryBlock::TryBind(VkBuffer buffer)
	{
		VkMemoryRequirements memory_requirements{};
		vkGetBufferMemoryRequirements(device.GetDevice(), buffer, &memory_requirements);

		auto maybe_block = TryFindFreeBlock(memory_requirements);
		if (maybe_block.IsSome())
		{
			auto & [block, index] = *maybe_block;
			vkBindBufferMemory(device.GetDevice(), buffer, memory, block.offset);
			used_block_regions.insert(used_block_regions.begin() + index, UsedBlockRegion{{block.offset, block.size}, variant<VkBuffer, VkImage>(buffer)});
			return true;
		}
		else
			return false;
	}

	bool MemoryBlock::TryUnbind(VkBuffer buffer)
	{
		auto it = find_if(used_block_regions.begin(), used_block_regions.end(), [buffer](UsedBlockRegion const & r) {
			auto b = get_if<VkBuffer>(&r.user);
			return b != nullptr && *b == buffer;
		});

		if (it != used_block_regions.end())
		{
			used_block_regions.erase(it);
			return true;
		}
		else
			return false;
	}

	bool MemoryBlock::TryBind(VkImage image)
	{
		VkMemoryRequirements memory_requirements{};
		vkGetImageMemoryRequirements(device.GetDevice(), image, &memory_requirements);

		auto maybe_block = TryFindFreeBlock(memory_requirements);
		if (maybe_block.IsSome())
		{
			auto & [block, index] = *maybe_block;
			vkBindImageMemory(device.GetDevice(), image, memory, block.offset);
			used_block_regions.insert(used_block_regions.begin() + index, UsedBlockRegion{{block.offset, block.size}, variant<VkBuffer, VkImage>(image)});
			return true;
		}
		else
			return false;
	}

	bool MemoryBlock::TryUnbind(VkImage image)
	{
		auto it = find_if(used_block_regions.begin(), used_block_regions.end(), [image](UsedBlockRegion const & r) {
			auto i = get_if<VkImage>(&r.user);
			return i != nullptr && *i == image;
		});

		if (it != used_block_regions.end())
		{
			used_block_regions.erase(it);
			return true;
		}
		else
			return false;
	}

	Optional<tuple<MemoryBlock::BlockRegion, usize>> MemoryBlock::TryFindFreeBlock(VkMemoryRequirements const & memory_requirements) const
	{
		if (size < memory_requirements.size)
			return OptionalNone;
		if (used_block_regions.size() == 0)
			return Optional<tuple<BlockRegion, usize>>(BlockRegion{0, memory_requirements.size}, 0);

		for (usize i = 0; i < used_block_regions.size(); i++)
		{
			auto current = used_block_regions.begin() + i;
			auto start = current->offset + current->size;
			auto alignment_offset = start % memory_requirements.alignment;
			if (alignment_offset > 0)
				start += memory_requirements.alignment - alignment_offset;

			auto next = used_block_regions.begin() + (i + 1);
			auto end = next == used_block_regions.end() ? this->size : next->offset;
			if (end >= start)
			{
				auto size = end - start;
				if (size > memory_requirements.size)
				{
					auto index = (current - used_block_regions.begin()) + 1;
					return Optional<tuple<BlockRegion, usize>>(BlockRegion{start, memory_requirements.size}, index);
				}
			}
		}

		return OptionalNone;
	}

	VkDeviceMemory MemoryBlock::CreateDeviceMemory(VkDevice device, u32 memory_type_index, usize size)
	{
		VkMemoryAllocateInfo allocate_info{
			.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
			.allocationSize = size,
			.memoryTypeIndex = memory_type_index,
		};

		VkDeviceMemory memory{};
		vkAllocateMemory(device, &allocate_info, nullptr, &memory);
		return memory;
	}
}
