#include "memory_transfer.hpp"

#include <cassert>
#include <cstring>

namespace gid_tech::rendering::implementation
{
	MemoryTransfer::MemoryTransfer(Device & device, usize stage_size) :
		device(device),
		stage_size(stage_size),
		buffer(CreateBuffer(device.GetDevice(), stage_size)),
		memory_requirements(GetMemoryRequirements(device.GetDevice(), buffer)),
		memory(CreateMemory(device.GetDevice(), buffer, device.GetMemoryIndex(memory_requirements.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_COHERENT_BIT), memory_requirements.size)),
		command_pool(CreateCommandPool(device.GetDevice(), device.GetQueueIndex(VK_QUEUE_GRAPHICS_BIT))),
		fence(CreateFence(device.GetDevice()))
	{
	}

	MemoryTransfer::~MemoryTransfer()
	{
		vkDestroyFence(device.GetDevice(), fence, nullptr);
		vkDestroyCommandPool(device.GetDevice(), command_pool, nullptr);
		vkDestroyBuffer(device.GetDevice(), buffer, nullptr);
		vkFreeMemory(device.GetDevice(), memory, nullptr);
	}

	void MemoryTransfer::Transfer(SrcData src_data, DstBufferData dst_data)
	{
		assert(src_data.size <= stage_size);

		void * stage_data{};
		vkMapMemory(device.GetDevice(), memory, 0, src_data.size, 0, &stage_data);
		memcpy(stage_data, src_data.ptr, src_data.size);
		VkMappedMemoryRange mapped_memory_range{
			.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
			.memory = memory,
			.offset = 0,
			.size = VK_WHOLE_SIZE,
		};
		vkFlushMappedMemoryRanges(device.GetDevice(), 1, &mapped_memory_range);
		vkUnmapMemory(device.GetDevice(), memory);

		VkCommandBuffer command_buffer = {0};
		VkCommandBufferAllocateInfo command_buffer_allocate_info{
			.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
			.commandPool = command_pool,
			.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
			.commandBufferCount = 1,
		};
		vkAllocateCommandBuffers(device.GetDevice(), &command_buffer_allocate_info, &command_buffer);

		VkCommandBufferBeginInfo command_buffer_begin_info = {.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO};
		vkBeginCommandBuffer(command_buffer, &command_buffer_begin_info);

		VkBufferCopy buffer_copy{
			.srcOffset = 0,
			.dstOffset = dst_data.offset,
			.size = src_data.size,
		};
		vkCmdCopyBuffer(command_buffer, buffer, dst_data.buffer, 1, &buffer_copy);

		vkEndCommandBuffer(command_buffer);

		VkSubmitInfo submit_info{
			.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
			.commandBufferCount = 1,
			.pCommandBuffers = &command_buffer,
		};
		auto queue_index = device.GetQueueIndex(VK_QUEUE_GRAPHICS_BIT);
		device.SubmitToQueue(queue_index, submit_info, fence);

		vkWaitForFences(device.GetDevice(), 1, &fence, VK_TRUE, UINT64_MAX);
		vkResetFences(device.GetDevice(), 1, &fence);
		vkFreeCommandBuffers(device.GetDevice(), command_pool, 1, &command_buffer);
	}

	void MemoryTransfer::Transfer(SrcData src_data, DstImageData dst_data)
	{
		assert(src_data.size <= stage_size);

		void * stage_data{};
		vkMapMemory(device.GetDevice(), memory, 0, src_data.size, 0, &stage_data);
		memcpy(stage_data, src_data.ptr, src_data.size);
		VkMappedMemoryRange memory_range{
			.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
			.memory = memory,
			.offset = 0,
			.size = VK_WHOLE_SIZE,
		};
		vkFlushMappedMemoryRanges(device.GetDevice(), 1, &memory_range);
		vkUnmapMemory(device.GetDevice(), memory);

		VkCommandBufferAllocateInfo command_buffer_allocate_info{
			.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
			.commandPool = command_pool,
			.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
			.commandBufferCount = 1,
		};
		VkCommandBuffer command_buffer{};
		vkAllocateCommandBuffers(device.GetDevice(), &command_buffer_allocate_info, &command_buffer);

		VkCommandBufferBeginInfo command_buffer_begin_info{.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO};
		vkBeginCommandBuffer(command_buffer, &command_buffer_begin_info);

		auto graphics_queue_index = device.GetQueueIndex(VK_QUEUE_GRAPHICS_BIT);

		VkImageMemoryBarrier barrier_pre{
			.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
			.srcAccessMask = 0,
			.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
			.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
			.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			.srcQueueFamilyIndex = graphics_queue_index,
			.dstQueueFamilyIndex = graphics_queue_index,
			.image = dst_data.image,
			.subresourceRange = VkImageSubresourceRange{
				.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
				.baseMipLevel = 0,
				.levelCount = 1,
				.baseArrayLayer = dst_data.layer_offset,
				.layerCount = dst_data.layer_count,
			},
		};
		vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier_pre);

		VkBufferImageCopy buffer_image_copy{
			.bufferOffset = 0,
			.imageSubresource = VkImageSubresourceLayers{.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT, .layerCount = 1},
			.imageOffset = dst_data.offset,
			.imageExtent = dst_data.extent,
		};
		vkCmdCopyBufferToImage(command_buffer, buffer, dst_data.image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &buffer_image_copy);

		VkImageMemoryBarrier barrier_post{
			.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
			.srcAccessMask = 0,
			.dstAccessMask = VK_ACCESS_SHADER_READ_BIT,
			.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			.newLayout = dst_data.new_layout,
			.srcQueueFamilyIndex = graphics_queue_index,
			.dstQueueFamilyIndex = graphics_queue_index,
			.image = dst_data.image,
			.subresourceRange = VkImageSubresourceRange{
				.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
				.baseMipLevel = 0,
				.levelCount = 1,
				.baseArrayLayer = dst_data.layer_offset,
				.layerCount = dst_data.layer_count,
			},
		};
		vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier_post);

		vkEndCommandBuffer(command_buffer);

		VkSubmitInfo submit_info{
			.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
			.commandBufferCount = 1,
			.pCommandBuffers = &command_buffer,
		};

		device.SubmitToQueue(graphics_queue_index, submit_info, fence);

		vkWaitForFences(device.GetDevice(), 1, &fence, VK_TRUE, UINT64_MAX);
		vkResetFences(device.GetDevice(), 1, &fence);
		vkFreeCommandBuffers(device.GetDevice(), command_pool, 1, &command_buffer);
	}

	VkBuffer MemoryTransfer::CreateBuffer(VkDevice device, VkDeviceSize size)
	{
		VkBufferCreateInfo create_info{
			.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
			.size = size,
			.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
			.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
		};

		VkBuffer buffer{};
		vkCreateBuffer(device, &create_info, nullptr, &buffer);
		return buffer;
	}

	VkMemoryRequirements MemoryTransfer::GetMemoryRequirements(VkDevice device, VkBuffer buffer)
	{
		VkMemoryRequirements memory_requirements{};
		vkGetBufferMemoryRequirements(device, buffer, &memory_requirements);
		return memory_requirements;
	}

	VkDeviceMemory MemoryTransfer::CreateMemory(VkDevice device, VkBuffer buffer, u32 memory_type_index, VkDeviceSize size)
	{
		VkMemoryAllocateInfo allocate_info{
			.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
			.allocationSize = size,
			.memoryTypeIndex = memory_type_index,
		};

		VkDeviceMemory memory{};
		vkAllocateMemory(device, &allocate_info, nullptr, &memory);
		vkBindBufferMemory(device, buffer, memory, 0);
		return memory;
	}

	VkCommandPool MemoryTransfer::CreateCommandPool(VkDevice device, u32 queue_index)
	{
		VkCommandPoolCreateInfo create_info{
			.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
			.queueFamilyIndex = queue_index,
		};

		VkCommandPool command_pool{};
		vkCreateCommandPool(device, &create_info, nullptr, &command_pool);
		return command_pool;
	}

	VkFence MemoryTransfer::CreateFence(VkDevice device)
	{
		VkFenceCreateInfo create_info{
			.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
		};
		VkFence fence;
		vkCreateFence(device, &create_info, nullptr, &fence);
		return fence;
	}
}
