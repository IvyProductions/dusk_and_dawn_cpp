#pragma once

#include <memory>
#include <mutex>
#include <vector>

#include <vulkan/vulkan.h>

#include <types/types.hpp>

#include "../device.hpp"

#include "memory_block.hpp"

using namespace std;

namespace gid_tech::rendering::implementation
{
	class MemoryCache
	{
	private:
		Device & device;

		usize const block_size;
		vector<unique_ptr<MemoryBlock>> blocks;

	public:
		MemoryCache(Device & device, usize block_size);
		~MemoryCache() = default;

		MemoryCache(MemoryCache const &) = delete;
		MemoryCache & operator=(MemoryCache const &) = delete;


		void Bind(VkBuffer buffer, VkMemoryPropertyFlags flags);
		void Unbind(VkBuffer buffer);
		void Bind(VkImage image, VkMemoryPropertyFlags flags);
		void Unbind(VkImage image);
	};
}
