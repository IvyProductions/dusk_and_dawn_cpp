#pragma once

#include <types/types.hpp>

using namespace gid_tech::types;

namespace gid_tech::collections
{
	template<typename T>
	struct IteratorForward
	{
		T * value;

		b8 operator==(IteratorForward<T> other)
		{
			return value == other.value;
		}
		b8 operator!=(IteratorForward<T> other)
		{
			return value != other.value;
		}

		T & operator*()
		{
			return *value;
		}
		T const & operator*() const
		{
			return *value;
		}

		T * operator->()
		{
			return value;
		}
		T const * operator->() const
		{
			return value;
		}

		IteratorForward<T> operator++()
		{
			return {.value = ++value};
		}
		IteratorForward<T> operator++(int)
		{
			return {.value = value++};
		}

		IteratorForward<T> operator--()
		{
			return {.value = --value};
		}
		IteratorForward<T> operator--(int)
		{
			return {.value = value--};
		}

		IteratorForward<T> & operator+=(usize amount)
		{
			*this = {.value = value + amount};
			return *this;
		}
		IteratorForward<T> & operator-=(usize amount)
		{
			*this = {.value = value - amount};
			return *this;
		}
	};

	template<typename T>
	struct IteratorBackward
	{
		T * value;

		b8 operator==(IteratorForward<T> other)
		{
			return value == other.value;
		}
		b8 operator!=(IteratorForward<T> other)
		{
			return value != other.value;
		}

		T & operator*()
		{
			return *value;
		}
		T const & operator*() const
		{
			return *value;
		}

		T * operator->()
		{
			return value;
		}
		T const * operator->() const
		{
			return value;
		}
		
		IteratorBackward<T> operator++()
		{
			return {.value = --value};
		}
		IteratorBackward<T> operator++(int)
		{
			return {.value = value--};
		}

		IteratorBackward<T> operator--()
		{
			return {.value = ++value};
		}
		IteratorBackward<T> operator--(int)
		{
			return {.value = value++};
		}

		IteratorBackward<T> & operator+=(usize amount)
		{
			*this = {.value = value - amount};
			return *this;
		}
		IteratorBackward<T> & operator-=(usize amount)
		{
			*this = {.value = value + amount};
			return *this;
		}
	};
}
