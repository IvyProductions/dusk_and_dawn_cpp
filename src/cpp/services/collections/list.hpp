#pragma once

#include "iterators.hpp"
#include "slice.hpp"
#include <heap/allocator.hpp>
#include <types/types.hpp>

using namespace gid_tech::types;
using namespace gid_tech::heap;

namespace gid_tech::collections
{
	struct ListInit
	{
		Allocator allocator{Allocator::default_allocator};
		usize block_size{16};
	};
	struct ListIndex
	{
		usize index;
	};
	template<typename TFunc>
	struct ListInitFunc
	{
		TFunc func;
		ListInitFunc(TFunc func) :
			func(func)
		{
		}
	};

	template<typename T>
	class List
	{
	private:
		Allocator allocator;
		usize block_size;

		usize count;
		T * elements;

	public:
		List(ListInit init = {});
		template<typename... TArgs>
		List(usize count, TArgs &&... args);
		template<typename... TArgs>
		List(ListInit init, usize count, TArgs &&... args);

		List(Slice<T> slice);
		List(ListInit init, Slice<T> slice);

		template<typename TFunc>
		List(usize count, ListInitFunc<TFunc> func);
		template<typename TFunc>
		List(ListInit init, usize count, ListInitFunc<TFunc> func);

		List(List<T> const & other);
		List(List<T> && other);

		~List();

		List<T> & operator=(List<T> const & other);
		List<T> & operator=(List<T> && other);

		T & operator[](usize index);
		T const & operator[](usize index) const;

		Slice<T> operator[](Range range);
		Slice<T const> operator[](Range range) const;

		b8 operator==(List<T> const & other) const;
		b8 operator!=(List<T> const & other) const;

		usize Count() const;
		b8 IsEmpty() const;

		T & First();
		T const & First() const;
		T & Last();
		T const & Last() const;

		IteratorForward<T> Begin();
		IteratorForward<T const> Begin() const;
		IteratorForward<T> End();
		IteratorForward<T const> End() const;

		IteratorBackward<T> RBegin();
		IteratorBackward<T const> RBegin() const;
		IteratorBackward<T> REnd();
		IteratorBackward<T const> REnd() const;

		template<typename... TArgs>
		T & Add(TArgs &&... args);
		template<typename... TArgs>
		T & Add(ListIndex index, TArgs &&... args);
		void Remove(ListIndex index);
		void Remove(T value);
		template<typename TPred>
		void Remove(TPred pred);

	private:
		usize GetBlockCount();

		T * Alloc();
		void Dealloc(T * e);
	};

	template<typename T>
	inline IteratorForward<T> begin(List<T> & list)
	{
		return list.Begin();
	}

	template<typename T>
	inline IteratorForward<T const> begin(List<T> const & list)
	{
		return list.Begin();
	}

	template<typename T>
	inline IteratorForward<T> end(List<T> & list)
	{
		return list.End();
	}

	template<typename T>
	inline IteratorForward<T const> end(List<T> const & list)
	{
		return list.End();
	}
}

#include "list.impl.hpp"
