#include "grappling_hook.hpp"

#include "box2d/box2d.h"
#include "grabable.hpp"

namespace lazy_rider::domain::entities
{
	GrapplingHook::GrapplingHook(b2World & physics_world, Vector<f32, 2> initalPosition, Vector<f32, 2> initalVelocity) :
		flightTime(2.f)
	{
		b2BodyDef bodyDef;
		bodyDef.type = b2_dynamicBody;
		bodyDef.position.Set(initalPosition.x, initalPosition.y);
		bodyDef.linearVelocity.Set(initalVelocity.x, initalVelocity.y);
		bodyDef.bullet = true;
		body = std::unique_ptr<b2Body, std::function<void(b2Body *)>>{
			physics_world.CreateBody(&bodyDef),
			[&physics_world](b2Body * b) {
				physics_world.DestroyBody(b);
			}};

		b2CircleShape dynamicCircle;
		dynamicCircle.m_radius = 0.5f;

		b2FixtureDef fixtureDef;
		fixtureDef.shape = &dynamicCircle;
		fixtureDef.density = 10;
		fixtureDef.friction = 0.3f;

		body->CreateFixture(&fixtureDef);
		body->GetUserData().anyObject = this;
	}

	Vector<f32, 2> GrapplingHook::Position() const
	{
		auto physicsPosition = body->GetPosition();
		return {physicsPosition.x, physicsPosition.y};
	}

	void GrapplingHook::Position(Vector<f32, 2> position)
	{
		auto angle = body->GetAngle();
		body->SetTransform(b2Vec2{position.x, position.y}, angle);
	}

	f32 GrapplingHook::Rotation() const
	{
		return body->GetAngle();
	}

	void GrapplingHook::Rotation(f32 angle)
	{
		auto physicsPosition = body->GetPosition();
		body->SetTransform(physicsPosition, angle);
	}

	void GrapplingHook::AttachTo(b2World & physics_world, Grabable * grabable)
	{
		Dettach(physics_world);

		if (grabable == nullptr)
			return;

		b2WeldJointDef jointDef;
		jointDef.Initialize(body.get(), grabable->body.get(), body.get()->GetPosition());

		joint = physics_world.CreateJoint(&jointDef);
		joint->GetUserData().RegisterDestroyEventCallback(reinterpret_cast<uintptr_t>(this), [this]() { joint = nullptr; });
		attachedGrababble = grabable;
	}

	void GrapplingHook::Dettach(b2World & physics_world)
	{
		if (joint == nullptr)
			return;

		joint->GetUserData().UnregisterDestroyEventCallback(reinterpret_cast<uintptr_t>(this));
		physics_world.DestroyJoint(joint);
		joint = nullptr;
		attachedGrababble = nullptr;
	}

	bool GrapplingHook::IsAttached() const
	{
		return joint != nullptr && attachedGrababble != nullptr;
	}

	Grabable * GrapplingHook::AttachedGrabable()
	{
		return attachedGrababble;
	}
}