#pragma once

#include "../services/algebra/vector.hpp"
#include "drink.hpp"
#include "trash.hpp"
#include <aggregates/optional.hpp>
#include <functional>
#include <memory>

class b2Body;
class b2Fixture;
class b2World;

using namespace gid_tech::aggregates;

namespace lazy_rider::domain::entities
{
	using gid_tech::algebra::Vector;

	struct Player
	{
		std::unique_ptr<b2Body, std::function<void(b2Body *)>> body;
		std::vector<Trash> trashInventory;
		std::vector<Drink> drinkInventory;
		bool hasWon{};

		Player(b2World & physics_world);

		Vector<f32, 2> Position() const;
		void Position(Vector<f32, 2>);
		f32 Rotation() const;
		void Rotation(f32);
		Vector<f32, 2> Velocity() const;

		void ApplyImpulse(Vector<f32, 2>);

		void AddTrash(Trash trash);
		void AddDrink(Drink drink);

		Optional<Trash> PopTrash();
	};
}
