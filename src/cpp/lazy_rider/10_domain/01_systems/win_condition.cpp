#include "win_condition.hpp"
#include "../00_entities/player.hpp"

namespace lazy_rider::domain::systems
{
    WinCondition::WinCondition(int trashToCollect,int drinksToCollect):trashToCollect(trashToCollect),drinksToCollect(drinksToCollect)
    {

    }

    void WinCondition::Tick(Player& player)
    {
        if(player.hasWon)
            return;

        player.hasWon = player.drinkInventory.size() >= drinksToCollect && player.trashInventory.size() >= trashToCollect;
    }
}