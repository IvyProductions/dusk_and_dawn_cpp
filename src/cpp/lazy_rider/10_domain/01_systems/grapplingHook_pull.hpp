#pragma once

#include <aggregates/optional.hpp>
#include <algebra/vector.hpp>
#include <functional>
#include <memory>

using namespace gid_tech::aggregates;

class b2Joint;
class b2World;

namespace lazy_rider::domain::game
{
	class Game;
}

namespace lazy_rider::domain::entities
{
	struct Player;
	struct GrapplingHook;
}

using namespace lazy_rider::domain::entities;

namespace lazy_rider::domain::systems
{
	class GrapplingHookPull
	{
	public:
		void Tick(f32 dt, b2World & physics_world, Player & player, Optional<GrapplingHook> & grapplingHook);
	};
}