#pragma once
#include "box2d/b2_world_callbacks.h"

namespace lazy_rider::domain::game
{
    class Game;
}

namespace lazy_rider::domain::systems
{
    using namespace lazy_rider::domain::game;

    class ContactResolver : public b2ContactListener
    {

    private:
        Game& game;
    public:
        ContactResolver( Game& game);

        void BeginContact(b2Contact* contact) override;
        void EndContact(b2Contact* contact) override;
    };    
}