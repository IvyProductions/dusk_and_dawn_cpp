#include "grabable_spawner.hpp"
#include "../00_entities/player.hpp"
#include "../03_game/game.hpp"
#include <cmath>

namespace lazy_rider::domain::systems
{
	GrabableSpawner::GrabableSpawner(f32 minRadius, f32 maxRadius, f32 trashChance, f32 drinkChance, f32 spawnTries, f32 spawnIntervalInSeconds, std::vector<Trash> trashPrototypes, Drink drinkPtorotype) :
		minRadius(minRadius),
		maxRadius(maxRadius),
		trashChance(trashChance),
		drinkChance(drinkChance),
		spawnTries(spawnTries),
		spawnIntervalInSeconds(spawnIntervalInSeconds),
		trashPrototypes(trashPrototypes),
		drinkPtorotype(drinkPtorotype),
		timeTillNextSpawn(0),
		mt(std::random_device()())
	{
	}

	void GrabableSpawner::Tick(f32 dt, Game & game, Player & player)
	{
		timeTillNextSpawn -= dt;
		if (timeTillNextSpawn > 0)
			return;

		timeTillNextSpawn = spawnIntervalInSeconds;

		auto playerPosition = player.Position();
		std::uniform_real_distribution<f32> dist_from_0_to_1(0.0f, 1.0f);

		std::uniform_real_distribution<f32> dist_range(minRadius, maxRadius);
		std::uniform_real_distribution<f32> dist_radiant(0, 2 * M_PI);
		std::uniform_int_distribution<int> dist_trashPrototype(0, trashPrototypes.size() - 1);
		auto spawnPositionGenerator = [&dist_range, &dist_radiant, this]() {
			f32 radiantAngle = dist_radiant(mt);
			f32 distance = dist_range(mt);
			f32 x = sin(radiantAngle) * distance;
			f32 y = cos(radiantAngle) * distance;

			return Vector<f32, 2>{x, y};
		};

		for (int i = 0; i < spawnTries; ++i)
		{
			f32 spawnChance = dist_from_0_to_1(mt);
			if (spawnChance < drinkChance)
			{
				auto position = spawnPositionGenerator() + playerPosition;
				game.AddGrabable(position, Vector<f32, 2>{}, drinkPtorotype);
			}

			if (spawnChance < trashChance && trashPrototypes.size() > 0)
			{
				auto trashToSpawnIndex = dist_trashPrototype(mt);
				auto position = spawnPositionGenerator() + playerPosition;
				game.AddGrabable(position, Vector<f32, 2>{}, trashPrototypes.at(trashToSpawnIndex));
			}
		}
	}
}
