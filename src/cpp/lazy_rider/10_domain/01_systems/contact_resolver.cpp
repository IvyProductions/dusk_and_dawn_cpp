#include "contact_resolver.hpp"

#include "../00_entities/grabable.hpp"
#include "../00_entities/grappling_hook.hpp"
#include "../00_entities/player.hpp"
#include "../03_game/game.hpp"

#include <box2d/box2d.h>
#include <variant>

using namespace lazy_rider::domain::entities;
using namespace gid_tech::aggregates;

namespace lazy_rider::domain::systems
{
	ContactResolver::ContactResolver(Game & game) :
		game(game)
	{
	}

	void ContactResolver::BeginContact(b2Contact * contact)
	{
		auto userDataA = contact->GetFixtureA()->GetBody()->GetUserData().anyObject;
		auto userDataB = contact->GetFixtureB()->GetBody()->GetUserData().anyObject;

		if (!userDataA.has_value() || !userDataB.has_value())
		{
			return;
		}

		std::any potentialPlayer = userDataA.type() == typeid(Player *) ? userDataA : userDataB;
		std::any potentialGrabable = userDataA.type() == typeid(Grabable *) ? userDataA : userDataB;
		std::any potentialGrapplingHook = userDataA.type() == typeid(GrapplingHook *) ? userDataA : userDataB;

		Player * player = potentialPlayer.type() == typeid(Player *) ? std::any_cast<Player *>(potentialPlayer) : nullptr;
		Grabable * grabable = potentialGrabable.type() == typeid(Grabable *) ? std::any_cast<Grabable *>(potentialGrabable) : nullptr;
		GrapplingHook * grapplingHook = potentialGrapplingHook.type() == typeid(GrapplingHook *) ? std::any_cast<GrapplingHook *>(potentialGrapplingHook) : nullptr;

		//handle GraplingHook attaching to stuff
		if (grapplingHook != nullptr && grabable != nullptr)
		{
			game.AddGrapplingHookAttach(grabable);
		}

		//handle GraplingHook returned to player, so destroy it.
		if (grapplingHook != nullptr && player != nullptr && (grapplingHook->IsAttached() || grapplingHook->flightTime <= 0))
		{
			game.AddGrapplingHookDestroy();
			if (grapplingHook->IsAttached())
			{
				auto grabblinHookAttachment = grapplingHook->AttachedGrabable();
				bool collected = true;
				if (std::holds_alternative<Trash>(grabblinHookAttachment->object))
				{
					auto & trash = std::get<Trash>(grabblinHookAttachment->object);
					collected = trash.isCollectable;
					if (collected)
					{
						game.AddPlayerTrashPickup(trash);
					}
				}

				if (std::holds_alternative<Drink>(grabblinHookAttachment->object))
					game.AddPlayerDrinkPickup(std::get<Drink>(grabblinHookAttachment->object));

				if (collected)
					game.AddRemoveGrabable(grabblinHookAttachment);
			}
		}
	}

	void ContactResolver::EndContact(b2Contact * contact)
	{
	}
}