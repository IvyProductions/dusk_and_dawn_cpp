#pragma once

#include <aggregates/optional.hpp>
#include <algebra/vector.hpp>

#include "../00_entities/player.hpp"

using namespace gid_tech::aggregates;

namespace lazy_rider::domain::entities
{
	struct Grabable;
	struct GrapplingHook;
}

class b2World;

namespace lazy_rider::domain::events
{
	using namespace lazy_rider::domain::entities;

	struct GrapplingHookAttach
	{
		Grabable * grabable;

		GrapplingHookAttach(Grabable * grabable);
		void Handle(b2World & physicsWorld, Optional<GrapplingHook> & grapplingHook);
	};

	struct GrapplingHookDetach
	{
		void Handle(b2World & physicsWorld, Player & player, Optional<GrapplingHook> & grapplingHook);
	};

	struct GrapplingHookStopFlight
	{
		void Handle(Optional<GrapplingHook> & grapplingHook);
	};

	struct GrapplingHookDestroy
	{
		void Handle(Optional<GrapplingHook> & grapplingHook);
	};
}
