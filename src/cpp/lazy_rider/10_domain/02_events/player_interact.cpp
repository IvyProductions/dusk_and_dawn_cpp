#include "player_interact.hpp"

#include "../00_entities/player.hpp"
#include "../03_game/game.hpp"
#include <aggregates/optional.hpp>
#include <box2d/b2_body.h>


using lazy_rider::domain::entities::Player;
using namespace gid_tech::aggregates;

namespace lazy_rider::domain::events
{
	PlayerGrapple::PlayerGrapple(Vector<f32, 2> relative_position) :
		relative_position(relative_position)
	{
	}

	void PlayerGrapple::Handle(b2World & physics_world, Player & player, Optional<GrapplingHook> & grapplingHook)
	{
		if (grapplingHook.IsSome())
			return;

		auto playerPosition = player.Position();
		auto playerVelocity = player.Velocity();

		auto playerMass = player.body->GetMass();
		auto trashMass = 10 * 0.78f;

		auto playerRatio = trashMass / (trashMass + playerMass);
		auto hookRatio = 1 - playerRatio;

		f32 speed = 100;
		auto dir = relative_position.Normalized();
		player.ApplyImpulse(dir * playerRatio * speed);
		grapplingHook.Emplace(
			physics_world,
			playerPosition + dir * -4.f,
			playerVelocity + dir * hookRatio * -speed);
	}


	PlayerThrow::PlayerThrow(Vector<f32, 2> relative_position) :
		relative_position(relative_position)
	{
	}

	void PlayerThrow::Handle(Player & player, Game & game)
	{
		if (player.trashInventory.size() > 0)
		{
			auto playerPosition = player.Position();
			auto playerVelocity = player.Velocity();

			auto playerMass = player.body->GetMass();
			auto trashMass = player.trashInventory.back().density * player.trashInventory.back().width * player.trashInventory.back().height;

			auto playerRatio = trashMass / (trashMass + playerMass);
			auto trashRatio = 1 - playerRatio;

			f32 speed = 30;
			auto dir = relative_position.Normalized();
			player.ApplyImpulse(dir * playerRatio * speed);
			game.AddGrabable(playerPosition + dir * -5, playerVelocity + dir * trashRatio * -speed, Trash{});
			player.trashInventory.pop_back();
		}
	}

	PlayerPickUpTrash::PlayerPickUpTrash(Trash item) :
		item(item)
	{
	}

	void PlayerPickUpTrash::Handle(Player & player)
	{
		player.AddTrash(item);
	}

	PlayerPickUpDrink::PlayerPickUpDrink(Drink item) :
		item(item)
	{
	}

	void PlayerPickUpDrink::Handle(Player & player)
	{
		player.AddDrink(item);
	}
}
