#pragma once

namespace lazy_rider::domain::game
{
	class Game;
}
namespace lazy_rider::domain::entities
{
	struct Grabable;
}

namespace lazy_rider::domain::events
{
    
	using namespace lazy_rider::domain::entities;
	using namespace lazy_rider::domain::game;

	struct GrabableRemove
	{
        Grabable* grabable;

        GrabableRemove(Grabable* grabable);
        void Handle(Game& game);
    };
}