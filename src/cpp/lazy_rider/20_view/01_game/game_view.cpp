#include "game_view.hpp"

#include <algebra/transform.hpp>
#include <algorithm>

namespace lazy_rider::view::game
{
	using namespace algebra;

	GameView::GameView(Game & game) :
		game(game),
		window_manager("Dusk and Dawn"),
		window(window_manager, "Dusk and Dawn", {1600, 900}),
		render_manager("Dusk and Dawn"),
		render_window(render_manager, rendering::Window::NativeWindowData{window_manager.GetImplementationHandle(), window.GetImplementationHandle()}),
		render_frame(make_shared<rendering::Frame>(render_manager, rendering::Size{1600, 900})),
		loading_cache(
			render_manager,
			[&](string const & name, MaterialData const & _) {
				auto transparent = name.find("transparent");
				if (transparent != string::npos)
					return transparent_pipeline;
				auto unlit = name.find("unlit");
				if (unlit != string::npos)
					return unlit_pipeline;
				return (transparent == string::npos) ? opaque_pipeline : transparent_pipeline;
			},
			[&](string const &) { return shared_ptr<Frame>(); }),
		opaque_pipeline(loading_cache.GetOrLoadPipeline("shaders/lazy_rider/lit.vert.spv", "shaders/lazy_rider/lit.frag.spv", 3, false)),
		transparent_pipeline(loading_cache.GetOrLoadPipeline("shaders/lazy_rider/lit.vert.spv", "shaders/lazy_rider/lit.frag.spv", 3, true)),
		unlit_pipeline(loading_cache.GetOrLoadPipeline("shaders/lazy_rider/unlit.vert.spv", "shaders/lazy_rider/unlit.frag.spv", 3, false)),
		scene(render_manager),
		main_light(
			"main light",
			Node::Transform{
				.rotation =
					Quaternion<f32>::FromAxisAngle({1, 0, 0}, Transform<f32>::ToRadians(10)) *
					Quaternion<f32>::FromAxisAngle({0, 1, 0}, Transform<f32>::ToRadians(-200)),
			},
			&scene,
			LightNode::Config{.type = LightNode::Config::Type::Directional, .color = {1.0, 1.0, 1.0}}),
		ambient_light(
			"ambient light",
			Node::Transform{.translation = {0, 0, 2}},
			&scene,
			LightNode::Config{.type = LightNode::Config::Type::Ambient, .color = {0.1, 0.1, 0.1}}),
		background_trash(loading::InstantiateSceneElements(
			loading_cache.GetOrLoadScene("gltf/asteroids/asteroid01.gltf"),
			Optional<variant<Scene *, Node *>>(&scene))),
		hook_prefab(loading_cache.GetOrLoadScene("gltf/rope/rope.gltf")),
		player_view(game, game.GetPlayer(), scene, render_frame, loading_cache)
	{
		scene.FindRootNode("asteroid").Value()->SetPosition({0, -6, 0});
	}

	void GameView::Tick(f32 dt)
	{
		window.Tick();

		auto & input = window.GetInput();
		if (input.ShouldClose() || input.GetKeyboardKeyUp(windowing::Input::Keyboard::Key::Escape))
			game.Stop();

		auto window_size = render_window.GetSize();
		auto frame_size = render_frame->GetSize();
		if (window_size != frame_size)
		{
			render_frame->SetSize(window_size);
			auto aspect = (f32) window_size.width / (f32) window_size.height;
			player_view.ChangeAspect(aspect);
		}

		// player
		player_view.Tick(dt, input);

		// grappling hook
		if (game.GetGrapplingHook().IsSome() && grappling_hook_view.IsNone())
			grappling_hook_view.Emplace(new GrapplingHookView(game.GetPlayer(), game.GetGrapplingHook().Value(), scene, loading_cache));
		else if (game.GetGrapplingHook().IsNone() && grappling_hook_view.IsSome())
			grappling_hook_view.Clear();

		if (grappling_hook_view.IsSome())
			grappling_hook_view.Value()->Tick();

		// grabbables
		for (auto & grabbable : game.GetGrabbables())
		{
			bool found = false;
			for (auto & grabbable_view : grabbable_views)
			{
				if (&grabbable_view->GetGrabbable() == grabbable.get())
				{
					found = true;
					break;
				}
			}
			if (!found)
				grabbable_views.emplace_back(make_unique<GrabbableView>(*grabbable, scene, loading_cache));
		}

		grabbable_views.erase(
			std::remove_if(
				grabbable_views.begin(),
				grabbable_views.end(),
				[this](unique_ptr<GrabbableView> const & grabbable_view) {
					return std::none_of(
						game.GetGrabbables().begin(),
						game.GetGrabbables().end(),
						[&grabbable_view](std::unique_ptr<Grabable> const & grabbable) {
							return grabbable.get() == &(grabbable_view->GetGrabbable());
						});
				}),
			grabbable_views.end());

		for (auto & grabbable_view : grabbable_views)
			grabbable_view->Tick();

		ambient_light.Translate({0, 0, 1 * dt});

		scene.Tick();
		render_window.Present(*render_frame);
	}
}
