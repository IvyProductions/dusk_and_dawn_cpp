#pragma once

#include <loading/cache.hpp>
#include <loading/scene_instance.hpp>
#include <scene/camera_node.hpp>
#include <scene/node.hpp>
#include <scene/scene.hpp>

#include <10_domain/00_entities/grappling_hook.hpp>
#include <10_domain/00_entities/player.hpp>
#include <10_domain/03_game/game.hpp>

using namespace std;
using namespace gid_tech;
using namespace scene;
using loading::LoadingCache;
using loading::SceneElements;
using loading::SceneInstance;

namespace lazy_rider::view::entities
{
	using namespace domain::entities;
	using namespace domain::game;

	class GrapplingHookView
	{
	private:
		Player const & player;
		GrapplingHook const & grappling_hook;

		Node root;
		SceneInstance grappling_hook_scene;
		Node * rope;

	public:
		GrapplingHookView(Player const & player, GrapplingHook const & grappling_hook, Scene & scene, LoadingCache & loading_cache);

		void Tick();
	};
}
