#include "grabbable_view.hpp"

#include <loading/scene_instance.hpp>

#include "material_replace.hpp"

namespace lazy_rider::view::entities
{
	GrabbableView::GrabbableView(Grabable const & grabbable, Scene & scene, LoadingCache & loading_cache) :
		grabbable(grabbable),
		root("grabbable", {}, &scene),
		instance(loading::InstantiateSceneElements(
			loading_cache.GetOrLoadScene(
				holds_alternative<Trash>(grabbable.object) ?
					(get<Trash>(grabbable.object).isCollectable ?
						 "gltf/grabbable/asteroid.gltf" :
						 "gltf/grabbable/capsule.gltf") :
					"gltf/grabbable/margarita.gltf"),
			&root))
	{
		ReplaceMaterials(dynamic_cast<ModelNode *>(root.GetChild(0)), loading_cache);
	}

	Grabable const & GrabbableView::GetGrabbable() const
	{
		return grabbable;
	}

	void GrabbableView::Tick()
	{
		auto pos = grabbable.Position();
		root.SetPosition({pos.x, 0, pos.y});
		root.SetRotation(Quaternion<f32>::FromAxisAngle({0, 1, 0}, grabbable.Rotation()));
	}
}
