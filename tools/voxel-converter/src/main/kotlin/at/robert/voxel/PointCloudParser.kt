package at.robert.voxel

import java.io.File

@ExperimentalUnsignedTypes
object PointCloudParser {

    fun parsePointCloud(file: File): VoxelObject {
        val lines = file.readLines().let { it.subList(11, it.size) }

        data class Point(
            val x: Int,
            val y: Int,
            val z: Int,
            val voxelMaterial: VoxelMaterial
        )

        val points = lines.map {
            val pointData = it.split(" ").map { it.toInt() }
            Point(
                pointData[0],
                pointData[1],
                pointData[2],
                ColoredVoxelMaterial(
                    pointData[3],
                    pointData[4],
                    pointData[5],
                )
            )
        }

        val minX = points.map { it.x }.minOrNull()!!
        val maxX = points.map { it.x }.maxOrNull()!!
        val minY = points.map { it.y }.minOrNull()!!
        val maxY = points.map { it.y }.maxOrNull()!!
        val minZ = points.map { it.z }.minOrNull()!!
        val maxZ = points.map { it.z }.maxOrNull()!!

        val movedPoints = points.map {
            it.copy(
                x = it.x - minX,
                y = it.y - minY,
                z = it.z - minZ,
            )
        }

        val lengthX = maxX - minX + 1
        val lengthY = maxY - minY + 1
        val lengthZ = maxZ - minZ + 1

        val materials = movedPoints.map { it.voxelMaterial }.distinct()
            .mapIndexed { index, voxelMaterial -> voxelMaterial to (index + 1) }.toMap()

        val bytes = UByteArray(lengthX * lengthY * lengthZ)
        movedPoints.forEach {
            require(it.x < lengthX)
            require(it.y < lengthY)
            require(it.z < lengthZ)
            bytes[it.x + it.y * lengthX + it.z * lengthX * lengthY] = materials[it.voxelMaterial]!!.toUByte()
        }

        val materialArray = listOf(EmptyVoxelMaterial) + materials.entries.sortedBy { it.value }.map { it.key }

        return VoxelObject(
            lengthX, lengthY, lengthZ, bytes, materialArray
        )
    }

}
