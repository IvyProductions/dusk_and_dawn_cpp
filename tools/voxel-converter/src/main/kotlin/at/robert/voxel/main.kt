package at.robert.voxel

import java.io.File

@ExperimentalUnsignedTypes
fun main(args: Array<String>) {
    require(args.size in 2..3) {
        "Usage: tool.jar <input.ply> <output.obj> [-noWrapping]"
    }
    val input = args[0]
    val output = args[1]
    val noWrap = when {
        args.size == 2 -> false
        args[2] == "-noWrapping" -> true
        else -> error("${args[2]} is not a known argument")
    }

    val obj = PointCloudParser.parsePointCloud(File(input))
    println("Got ${obj.data.size} voxels")
    println("Got ${obj.voxelMaterials.size} materials (${obj.voxelMaterials})")
    require(obj.data.any {
        val uByte: UByte = 0u
        it != uByte
    })
    require(obj.data.all {
        it.toInt() in obj.voxelMaterials.indices
    })
    require(obj.voxelMaterials.indices.all {
        it == 0 || it in obj.data.map { it.toInt() }
    })
    obj.exportObj(File(output).let {
        if (it.isDirectory) {
            File(it, File(input).nameWithoutExtension + ".obj")
        } else {
            it
        }
    }, noWrap = noWrap)
}
