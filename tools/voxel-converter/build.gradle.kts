import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.10"
    id("application")
}

group = "at.robert"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

application{
    mainClassName = "at.robert.voxel.MainKt"
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "11"
}
